<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'auth'], function() {
    Route::get('/', function () {

        return view('layouts.pages.home.index',
         [
             'loanTypes' => \App\Models\LoanType::latest()->get(),
             'courses' => \App\Models\Course::latest()->get()
         ]);
    });
    //clients
    Route::resource('clients', 'ClientController');
    Route::put('clients-update-status/{code}', 'ClientController@updateStatus')->name('clients.update-status');
    //loans-types
    Route::resource('loan-types', 'LoanTypeController');
    //loans
    Route::resource('loans', 'LoanController');
    
    Route::get('loan-statement/{code}', 'LoanController@genLoanStatement')->name('loan-statement');
    Route::put('loans-status/{code}', 'LoanController@updateStatus')->name('loans.status');
    Route::resource('loan-repayments', 'LoanRepaymentController');
    Route::get('loan-receipt/{code}', 'LoanRepaymentController@paymentReceipt')->name('loan-receipt');
    Route::group(['prefix' => 'academy'], function() {
        
        Route::get('/', function() {
            
            return view('layouts.pages.academy.index', ['courses' => \App\Models\Course::latest()->get()]);
        })->name('academy.index');
        Route::resource('courses', 'CourseController');
        Route::resource('students', 'StudentController');
        Route::resource('student-course', 'StudentCourseController');
        Route::get('receipt-course-payment/{code}', 'StudentCourseController@paymentReceipt')->name('receipt-course-payment');
        Route::get('student-course-payments', 'StudentCourseController@coursePayments')->name('student-course-payments.index');
        Route::post('student-course-partial', 'StudentCourseController@partialPayment')->name('student-course.partial');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
