<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
   <title>Login Shanyvin Limited</title>

  <!-- Favicons-->
  <link rel="icon" href="" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" >
  <meta name="msapplication-TileImage" >
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="/assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
    <link href="/assets/css/custom-style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- CSS style Horizontal Nav (Layout 03)-->    
    <link href="/assets/css/style-horizontal.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/assets/css/page-center.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="/assets/css/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  
</head>

<body class="cyan">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->



  <div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
      <form class="login-form" method="POST" action="{{ route('login') }}" >
        @csrf
        <div class="row">
          <div class="input-field col s12 center">
            <img src="/assets/images/logo.jpg" alt="" class="circle responsive-img valign profile-image-login">
            <p class="center login-form-text">Shanyvin Limited</p>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="email" name="email" type="email" required>
            <label for="username" class="center-align">Email address</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password" type="password" name="password" required>
            <label for="password">Password</label>
          </div>
        </div>
        <div class="row">          
          <div class="input-field col s12 m12 l12  login-text">
              <input type="checkbox" id="remember-me" name="remember" id="remember" />
              <label for="remember-me">Remember me</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <button class="btn waves-effect waves-light col s12">Login</button>
          </div>
        </div>

      </form>
    </div>
  </div>



  <!-- ================================================
    Scripts
    ================================================ -->

  <!-- jQuery Library -->
  <script type="text/javascript" src="/assets/js/jquery-1.11.2.min.js"></script>
  <!--materialize js-->
  <script type="text/javascript" src="/assets/js/materialize.js"></script>
  <!--prism-->
  <script type="text/javascript" src="/assets/js/prism.js"></script>
  <!--scrollbar-->
  <script type="text/javascript" src="/assets/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

  <!--plugins.js - Some Specific JS codes for Plugin Settings-->
  <script type="text/javascript" src="/assets/js/plugins.js"></script>

</body>

</html>