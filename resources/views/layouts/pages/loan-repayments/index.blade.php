@extends('layouts.app')

@section('body')
<section id="content">
        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper" class=" grey lighten-3">
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Loan repayments</h5>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">List of loan repayments</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->
<div class="container">
    <div class="card-panel">
        <!-- profile-page-wall -->
 <div id="profile-page-wall" class="col s12 m9">
    <!-- profile-page-wall-share -->
    <div id="profile-page-wall-share" class="row">
        <div class="col s12">
                <div class="card-title">
                    <h5> Repayments </h5>
                </div>
                    <table class="data-table responsive-table">
                            <tbody>
                                <?php $total = 0; ?>
                                @foreach($pendingRepayments as $key => $repayment)
                                <?php $total += $repayment->amount; ?>
                                <tr>
                                <td class="center-align">{{ $key+1 }}</td>
                                <td class="center-align">{{ ucwords($repayment->loan->client->first_name .' '.$repayment->loan->client->last_name) }}</td>
                                    <td class="center-align">{{ $repayment->loan->client->membership_number }}</td>
                                    <td class="center-align">{{ $repayment->payment_mode }}</td>
                                    <td class="center-align">{{ number_format($repayment->amount, 2) }}</td>
                                    <td class="center-align">{{ \Carbon\Carbon::parse($repayment->created_at)->format('Y-M-d') }}</td>
                                    <td class="center-align">
                                            {{ $repayment->transaction_code }}
                                    </td>
                                    <td>
                                    <a href="{{ route('loan-receipt', $repayment->code)}}" class="btn btn-xs btn-info" title="repayment receipt"><i class="mdi-editor-attach-file"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                            <thead>
                                <tr>
                                      <th class="center-align">No.</th>
                                      <th class="center-align">Client</th>
                                      <th class="center-align">Membership no</th>
                                      <th class="center-align">Payment mode</th>
                                <th class="center-align">Amount-KES {{ number_format($total, 2)}}</th>
                                      <th class="center-align">Date of payment</th>
                                      <th class="center-align">Transaction code</th>
                                      <th class="center-align">Receipt</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                      <th class="center-align">No.</th>
                                      <th class="center-align">Client</th>
                                      <th class="center-align">Membership no</th>
                                      <th class="center-align">Payment mode</th>
                                      <th class="center-align">Amount-KES {{ number_format($total, 2)}}</th>
                                      <th class="center-align">Date of payment</th>
                                      <th class="center-align">Transaction code</th>
                                      <th class="center-align">Receipt</th>
                                </tr>
                            </tfoot>
                          </table>
        </div>
    </div>
    <!--/ profile-page-wall-share -->
  </div>
  <!--/ profile-page-wall -->
</div>
</section>
@endsection
@section('css')
    <!-- Custome CSS-->    
    <link href="/assets/css/custom-style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="assets/js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
@endsection
@section('js')
<script type="text/javascript" src="/assets/js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
<script>
    $(function(){
        $('.data-table').DataTable();
    })
</script>
@endsection
