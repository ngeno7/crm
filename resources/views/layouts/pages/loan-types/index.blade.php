@extends('layouts.app')

@section('body')
<section id="content">
        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper" class=" grey lighten-3">
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Loan Types</h5>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">List of Loan Types</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->
    <div class="row">
        <div class="col s12 m6 l6">
            <div class="card-panel">
            <form class="" action="{{ route('loan-types.store')}}" method="POST">
                        @csrf
                            <div class="row">
                              <div class="input-field col s6">
                                <input id="name" name="name" type="text" required>
                                <label for="name">Name</label>
                              </div>
                                <div class="input-field col s6">
                                  <input id="interest" name="interest" type="number" required>
                                  <label for="interest">Interest %(per month)</label>
                                </div>
                            </div>
                            <div class="row">
                              <div class="input-field col s12">
                                <textarea id="description" name="description" class="materialize-textarea"></textarea>
                                <label for="description">Description</label>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <button class="btn green waves-effect waves-light right" type="submit" name="action">Save
                                    <i class="mdi-action-done-all right"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                    </form>
            </div>
        </div>
        <div class="col s12 m6 l6">
            <div class="card-panel">
                    <h4 class="header">Loan Types List</h4>
                  <ul id="projects-collection" class="collection">
                    <li class="collection-item avatar">
                      <i class="mdi-file-folder circle light-blue"></i>
                      <span class="collection-header">Loans</span>
                      <a href="#" class="secondary-content"><i class="mdi-action-shopping-basket"></i></a>
                    </li>
                    @foreach($loanTypes as $key => $loanType)
                    <li class="collection-item">
                      <div class="row">
                        <div class="col s4">
                        <p class="collections-title">{{ $loanType->name }}</p>
                        <p class="collections-title">Interest <b>{{ $loanType->interest }}%</b></p>
                        </div>
                        <div class="col s8">
                            <p class="collections-content">
                                {{ $loanType->description }}
                              </p>
                              <a href="{{ route('loan-types.show', $loanType->code) }}" title="view loans available" class="btn-floating waves-effect waves-light btn-small blue">
                                  <i class="mdi-action-visibility"></i>
                              </a>
                        </div>
                      </div>
                    </li>
                    @endforeach
                  </ul>
            </div>
        </div>
</section>
@endsection