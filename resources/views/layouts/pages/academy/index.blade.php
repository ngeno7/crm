@extends('layouts.app')
@section('body')
<section id="content">

    <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Forex academy</h5>
            <ol class="breadcrumb"> 
              <li><a href="/">Home</a>
              </li>
              <li class="active">Shanyvin academy</li>
            </ol>
          </div>
        </div>
      </div>
        <!--start container-->
        <div class="container">

            <!--card stats start-->
            <div id="card-stats">
                <div class="row">
                    <div class="col s12 m4 l4">
                        <div class="card">
                            <div class="card-content  green white-text">
                                <p class="card-stats-title"><i class="mdi-social-group-add" style="font-size: 35px"></i></p>
                                <h4 class="card-stats-number">Students</h4>
                            </div>
                            <div class="card-action  green darken-1 center">
                                <a class="btn btn-small waves-light yellow black-text" href="{{ route('students.index') }}"><i class="mdi-action-account-child"></i> View</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4 l4">
                        <div class="card">
                            <div class="card-content light-green white-text">
                                <p class="card-stats-title"><i class="mdi-communication-vpn-key" style="font-size: 35px"></i></p>
                                <h4 class="card-stats-number">Fee payments</h4>
                            </div>
                            <div class="card-action light-green center">
                                <a  href="{{ route('student-course-payments.index') }}" class="btn waves-effect waves-light black btn-small white-text"><i class="mdi-social-share"></i> View</a>
                            </div>
                        </div>
                    </div>                           
                    <div class="col s12 m4 l4">
                        <div class="card">
                            <div class="card-content blue white-text">
                                <p class="card-stats-title"><i class="mdi-social-poll" style="font-size: 35px"></i></p>
                                <h4 class="card-stats-number">Statistics</h4>
                                </p>
                            </div>
                            <div class="card-action blue center">
                                    <a class="btn waves-effect waves-light yellow darken-4 btn-small white-text"><i class="mdi-social-poll"></i> View</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--card stats end-->
            <div id="work-collections">
                <div class="row">
                    <div class="col s12 m12 l6">
                  <form action="{{ route('courses.store') }}" method="post">
                    @csrf
                        <ul id="projects-collection" class="collection">
                            <li class="collection-item avatar">
                                <i class="mdi-action-settings circle light-blue darken-2"></i>
                                <span class="collection-header">Add course</span>
                                <p>Forex academy</p>
                                <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a>
                            </li>
                            <li class="collection-item">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="name" name="name" type="text" required value="{{ old('name') }}">
                                        <label for="name">Name</label>
                                        <span class="helper-text" data-error="wrong">
                                                {{ $errors->has('name') ? $errors->first('name') : ''}}
                                        </span>
                                </div>
                                <div class="input-field col s6">
                                    <input id="cost" name="cost" type="number" required value="{{ old('cost') }}">
                                    <label for="cost">Fee - KES</label>
                                    <span class="helper-text" data-error="wrong">
                                            {{ $errors->has('cost') ? $errors->first('cost') : '' }}
                                    </span>
                                </div>
                                </div>
                            </li>
                            <div class="input-field col s12">
                                <textarea id="description" name="description" class="materialize-textarea"></textarea>
                                <label for="description">Description</label>
                            </div>
                            <li class="collection-item">
                                <button class="btn btn-small green right"><i class="mdi-action-done-all left"></i> Save</button><br>
                            </li>
                        </ul>
                    </form>
                    </div>
                    <div class="col s12 m12 l6">
                        <ul id="issues-collection" class="collection">
                            <li class="collection-item avatar">
                                <i class="mdi-editor-attach-file circle red darken-2"></i>
                                <span class="collection-header">Academy</span>
                                <p>Courses Offered In Forex Academy</p>
                                <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a>
                            </li>
                            @foreach($courses as $course)
                            <li class="collection-item">
                                <div class="row">
                                    <div class="col s10">
                                    <p class="collections-title"><strong>{{ $course->name }}</strong></p>
                                    <p class="collections-content">{{ $course->description }}</p>
                                    </div>
                                    <div class="col s2">
                                    <span class="task-cat pink accent-2">KES {{ number_format($course->cost, 2) }}</span>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end container-->
    </section>
@endsection