@extends('layouts.app')

@section('body')
<section id="content">
        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper" class=" grey lighten-3">
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Course payments</h5>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="{{ route('academy.index') }}">Academy</a></li>
                <li class="active">List of course payments</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->
<div class="container">
<div class="card-panel">
        <!-- profile-page-wall -->
 <div id="profile-page-wall" class="col s12">
    <!-- profile-page-wall-share -->
    <div class="row">
        <!-- UpdateStatus-->
        <div id="UpdateStatus" class=" col s12  grey lighten-4">
            <div class="card-panel">
                <table class="data-table responsive-table">
                  <thead>
                      <tr>
                          <th class="center-align">No.</th>
                          <th class="center-align">Name</th>
                          <th class="center-align">Course</th>
                          <th class="center-align">Amount</th>
                          <th class="center-align">Date of Payment</th>
                          <th class="center-align">Action</th>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                            <th class="center-align">No.</th>
                            <th class="center-align">Name</th>
                            <th class="center-align">Course</th>
                            <th class="center-align">Amount</th>
                            <th class="center-align">Date of Payment</th>
                            <th class="center-align">Action</th>
                      </tr>
                  </tfoot>
               
                  <tbody>
                      @foreach($payments as $key => $payment)
                      <tr>
                          <td class="center-align">{{ $key+1 }}</td>
                        <td class="center-align">
                          {{ ucwords($payment->studentCourse->student->first_name .' '. $payment->studentCourse->student->last_name) }}
                        </td>
                          <td class="center-align">
                            {{ $payment->studentCourse->course->name }}
                          </td>
                          <td class="center-align">
                            KES {{ number_format($payment->amount, 2) }}
                          </td>
                          <td class="center-align">
                                {{ \Carbon\Carbon::parse($payment->created_at)->format('Y-M-d') }}
                          </td>
                          <td>
                            <a class="btn-floating green" title="download receipt"><i class="large mdi-editor-vertical-align-bottom"></i></a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
          </div>
        </div>
      </div>
    </div>
    <!--/ profile-page-wall-share -->
  </div>
  <!--/ profile-page-wall -->
</div>
</section>
@endsection
@section('css')
    <!-- Custome CSS-->    
    <link href="/assets/css/custom-style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/assets/js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
@endsection
@section('js')
<script type="text/javascript" src="/assets/js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
<script>
    $(function(){
        $('.data-table').DataTable();
    })
</script>
@endsection
