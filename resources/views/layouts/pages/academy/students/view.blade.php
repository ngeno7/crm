@extends('layouts.app')

@section('body')
<section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper" class=" grey lighten-3">
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Loan Details</h5>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="{{ route('students.index') }}">Student registrations</a></li>
                    <li class="active">Student Details</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
          <div class="row">
              <div class="col s12 m4">
                <ul id="profile-page-about-details" class="collection z-depth-1">
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s6 black-text darken-4"><i class="mdi-action-account-circle"></i> Name</div>
                    <div class="col s6 black-text text-darken-4 center-align">{{ ucwords($profile->first_name.' '.$profile->last_name) }}</div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s6 black-text darken-1"><i class="mdi-action-settings-phone"></i> Phone number</div>
                      <div class="col s6 black-text text-darken-4 center-align">{{ $profile->phone_number }}</div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s6 black-text darken-1"><i class="mdi-action-speaker-notes"></i>Email address</div>
                      <div class="col s6 black-text text-darken-4 center-align">{{ $profile->email_address }}</div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s6 black-text darken-1"><i class="mdi-file-folder-open"></i> ID number</div>
                      <div class="col s6 black-text text-darken-4 center-align">{{ $profile->id_number }}</div>
                    </div>
                  </li>
                </ul>
              </div>
              <!-- profile-page-wall -->
              <div id="profile-page-wall" class="col s12 m8">
                    <!-- profile-page-wall-share -->
                    <div id="profile-page-wall-share" class="row">
                      <div class="col s12">
                        <ul class="tabs tab-profile z-depth-1 light-green">
                          <li class="tab col s3"><a class="white-text waves-effect waves-light active" href="#UpdateStatus"><i class="mdi-action-swap-vert-circle medium"></i>Student course Reg</a>
                          </li>
                          <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#AddPhotos"><i class="mdi-action-swap-horiz medium"></i>Course Partial payments</a>
                          </li>
                          
                          <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#CreateAlbum"><i class="mdi-action-view-list medium"></i> Course payment history</a>
                          </li>                      
                        </ul>
                        <!-- UpdateStatus-->
                        <div id="UpdateStatus" class="tab-content col s12  grey lighten-4">
                          <div class="row">
                             <div class="col s12 m12 l12">
                                  <ul id="issues-collection" class="collection">
                                      <li class="collection-item avatar">
                                          <i class="mdi-action-view-week circle red darken-2"></i>
                                          <span class="collection-header"><strong>Course registration</strong></span>
                                          <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a>
                                      </li>
                                    <form action="{{ route('student-course.store') }}" method="post">
                                      @csrf
                                    <input type="hidden" name="student_id" value="{{ $profile->id }}">
                                      <div class="card-panel">
                                          <div class="row">
                                            <div class="col s6"><br>
                                             <select name="course_id" id="course_id" required>
                                                  <option disabled selected placeholder>Select course...</option>
                                                   @foreach($courses as $course)
                                                  <option value="{{ $course->id }}">{{ $course->name }}</option>
                                                   @endforeach          
                                              </select>
                                            </div>
                                            <div class="input-field col s6">
                                                 <input id="amount" name="amount" type="number" value="{{ old('amount') }}" required>
                                                 <label for="amount">Amount -KES</label>
                                                 <span class="helper-text" data-error="wrong">
                                                      {{ $errors->has('amount') ? $errors->first('amount') : '' }}
                                                 </span>
                                              </div>
                                          </div>
                                          <div class="row">
                                            <div class="input-field col s6">
                                              <input id="comment" name="comment" type="text">
                                              <label for="comment">Comment</label>
                                            </div>
                                            <div class="col s6">
                                                <label for="payment_mode">Payment Mode</label>
                                              <select name="payment_mode" id="payment_mode" required>
                                                  <option disabled selected placeholder>Select payment mode...</option>
                                                  <option value="Bank">Bank</option>
                                                  <option value="Mpesa">Mpesa</option>
                                                  <option value="Cash deposit">Cash Deposit</option>
                                              </select>
                                          </div>
                                          </div>
                                          <div class="row">
                                            <div class="input-field col s6">
                                              <input id="transaction_code" name="transaction_code" type="text" required>
                                              <label for="transaction_code">Transaction code</label>
                                            </div>
                                            <div class="col s6"><br>
                                              <button class="btn btn-sm green" type="submit">Save
                                                  <i class="mdi-action-done-all left"></i>
                                              </button>
                                            </div>
                                          </div>
                                        </div>
                                      </form>
                                  </ul>
                              </div>
                          </div>
                        </div>
                        <!-- AddPhotos -->
                        <div id="AddPhotos" class="tab-content col s12  grey lighten-4">
                          <div class="row">
                              <div class="card-panel">
                                <div class="card-title"><h6><strong>Add new repayment</strong></h6></div>
                                <form action="{{ route('student-course.partial') }}" method="post">
                                    @csrf
                                  <input type="hidden" name="student_id" value="{{ $profile->id }}">
                                    <div class="card-panel">
                                        <div class="row">
                                          <div class="col s6"><br>
                                           <select name="student_course_id" id="student_course_id" required>
                                                <option disabled selected placeholder>Select course...</option>
                                                 @foreach($profile->courses as $regcourse)
                                                <option value="{{ $regcourse->id }}">{{ $regcourse->course->name }}</option>
                                                 @endforeach          
                                            </select>
                                          </div>
                                          <div class="input-field col s6">
                                               <input id="amount" name="amount" type="number" value="{{ old('amount') }}" required>
                                               <label for="amount">Amount -KES</label>
                                               <span class="helper-text" data-error="wrong">
                                                    {{ $errors->has('amount') ? $errors->first('amount') : '' }}
                                               </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                          <div class="input-field col s6">
                                            <input id="comment" name="comment" type="text">
                                            <label for="comment">Comment</label>
                                          </div>
                                          <div class="col s6">
                                              <label for="payment_mode">Payment Mode</label>
                                            <select name="payment_mode" id="payment_mode" required>
                                                <option disabled selected placeholder>Select payment mode...</option>
                                                <option value="Bank">Bank</option>
                                                <option value="Mpesa">Mpesa</option>
                                                <option value="Cash deposit">Cash Deposit</option>
                                            </select>
                                        </div>
                                        </div>
                                        <div class="row">
                                          <div class="input-field col s6">
                                            <input id="transaction_code" name="transaction_code" type="text" required>
                                            <label for="transaction_code">Transaction code</label>
                                          </div>
                                          <div class="col s6"><br>
                                            <button class="btn btn-sm green" type="submit">Save
                                                <i class="mdi-action-done-all left"></i>
                                            </button>
                                          </div>
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                          </div>
                        </div>
                        <div id="CreateAlbum" class="tab-content col s12  grey lighten-4">
                          <div class="row">
                              <div class="card-panel">
                                <table class="data-table responsive-table">
                                  <thead>
                                      <tr>
                                          <th class="center-align">No.</th>
                                          <th class="center-align">Course</th>
                                          <th class="center-align">Amount</th>
                                          <th class="center-align">Payment mode</th>
                                          <th class="center-align">Date of payment</th>
                                          <th class="center-align">Receipt</th>
                                      </tr>
                                  </thead>
                                  <tfoot>
                                      <tr>
                                        <th class="center-align">No.</th>
                                        <th class="center-align">Course</th>
                                        <th class="center-align">Amount</th>
                                        <th class="center-align">Payment mode</th>
                                        <th class="center-align">Date of payment</th>
                                        <th class="center-align">Receipt</th>
                                      </tr>
                                  </tfoot>
                                  <tbody>
                                    @foreach($coursePaymentHistory as $key => $payment)
                                    <tr>
                                      <td class="center-align">{{ $key+1 }}</td>
                                      <td class="center-align">{{ $payment->studentCourse->course->name }}</td>
                                      <td class="center-align">KES: {{ number_format($payment->amount,2) }}</td>
                                      <td class="center-align">{{ $payment->payment_mode }}</td>
                                      <td class="center-align">{{ \Carbon\Carbon::parse($payment->created_at)->format('Y-M-d') }}</td>
                                      <td class="center-align">
                                      <a class="btn-floating green" href="{{ route('receipt-course-payment', $payment->code)}}"><i class="large mdi-editor-vertical-align-bottom"></i></a>
                                      </td>
                                    </tr>
                                    @endforeach
                                  </tbody>
                                </table>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--/ profile-page-wall-share -->
                  </div>
                  <!--/ profile-page-wall -->
                </div>
 </section>
@endsection
@section('css')
<style>
  .medium {
    font-size: 25px !important;
  }
</style>
@endsection