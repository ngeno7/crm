@extends('layouts.app')

@section('body')
<section id="content">
        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper" class=" grey lighten-3">
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Students</h5>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
            <li><a href="{{ route('students.index') }}">Students</a></li>
                <li class="active">Add student</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->
<div class="container">
    <div class="card-panel">
        <div class="card-title">
            <h5 class="left-align">Add student</h5>
        </div>
        <div class="s8 offset-s2">
            <div class="row">
                <form class="col s12" method="post" action="{{ route('students.store') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="input-field col s4">
                            <input id="first_name" name="first_name" type="text" required value="{{ old('last_name') }}">
                            <label for="first_name">First Name</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('first_name') ? $errors->first('first_name') : ''}}
                            </span>
                        </div>
                        <div class="input-field col s4">
                            <input id="last_name" name="last_name" type="text" required value="{{ old('last_name') }}">
                            <label for="last_name">Last Name</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('last_name') ? $errors->first('last_name') : ''}}
                            </span>
                        </div>
                        <div class="input-field col s4">
                            <input id="middle_name" name="middle_name" type="text" value="{{ old('middle_name') }}">
                            <label for="middle_name">Middle Name</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('middle_name') ? $errors->first('middle_name') : ''}}
                            </span>
                        </div>
                     </div>
                     <div class="row">
                            <div class="input-field col s4">
                                <input id="id_number" name="id_number" type="number" required value="{{ old('id_number') }}">
                                <label for="id_number">ID Number</label>
                                <span class="helper-text" data-error="wrong">
                                        {{ $errors->has('id_number') ? $errors->first('id_number') : ''}}
                                </span>
                            </div>
                            <div class="input-field col s4">
                                <input id="email" name="email_address" type="text" required value="{{ old('email_address') }}">
                                <label for="email_address">Email</label>
                                <span class="helper-text" data-error="wrong">
                                        {{ $errors->has('email_address') ? $errors->first('email_address') : ''}}
                                </span>
                            </div>
                            <div class="input-field col s4">
                                <input id="phone_number" name="phone_number" type="number" value="{{ old('phone_number') }}" required>
                                <label for="phone_number">Phone Number</label>
                                <span class="helper-text" data-error="wrong">
                                        {{ $errors->has('phone_number') ? $errors->first('phone_number') : ''}}
                                </span>
                            </div>
                     </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="address" name="address" type="text" value="{{ old('address') }}">
                            <label for="address">Address</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('address') ? $errors->first('address') : ''}}
                            </span>
                        </div>
                        <div class="input-field col s6">
                            <input id="nok_name" name="nok_name" type="text" value="{{ old('nok_name') }}">
                            <label for="nok_name">Next of kin name</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('nok_name') ? $errors->first('nok_name') : ''}}
                            </span>
                        </div>
                    </div>
                    <div class="row">
                            <div class="input-field col s6">
                                    <input id="nok_phone_number" name="nok_phone_number" type="number" value="{{ old('nok_phone_number') }}">
                                    <label for="nok_phone_number">NOK Phone Number</label>
                                    <span class="helper-text" data-error="wrong">
                                            {{ $errors->has('nok_phone_number') ? $errors->first('nok_phone_number') : ''}}
                                    </span>
                                </div>
                            <div class="input-field col s6">
                            <input id="nok_email_address" name="nok_email_address" type="email" value="{{ old('nok_email_address') }}">
                                <label for="nok_email_address">NOK Email Address</label>
                                <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('nok_email_address') ? $errors->first('nok_email_address') : ''}}
                                </span>
                            </div>
                    </div>
                      <div class="row">
                            <div class="input-field col s12">
                               <button class="btn green waves-effect waves-light right" type="submit">Save
                                    <i class="mdi-action-done-all left"></i>
                                </button>
                            </div>
                     </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
@endsection