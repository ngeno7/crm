@extends('layouts.app')

@section('body')
<section id="content">
        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper" class=" grey lighten-3">
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Students</h5>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="{{ route('academy.index') }}">Academy</a></li>
                <li class="active">List of students</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->
<div class="container">
    <div class="card-panel">
        <div class="card-title">
        <a href="{{ route('students.create') }}" class="waves-effect waves-light btn-small green  btn right"><i class="mdi-content-add left"></i>Student</a>
        </div><br><br>
        <!-- profile-page-wall -->
 <div id="profile-page-wall" class="col s12 m12 l12">
    <!-- profile-page-wall-share -->
    <div class="row">
        <!-- UpdateStatus-->
        <div id="UpdateStatus" class=" col s12  grey lighten-4">
            <div class="card-panel">
                <table class="data-table responsive-table">
                  <thead>
                      <tr>
                          <th class="center-align">No.</th>
                          <th class="center-align">Name</th>
                          <th class="center-align">Email address</th>
                          <th class="center-align">Phone number</th>
                          <th class="center-align">Id number</th>
                          <th class="center-align">Action</th>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th class="center-align">No.</th>
                          <th class="center-align">Name</th>
                          <th class="center-align">Email address</th>
                          <th class="center-align">Phone number</th>
                          <th class="center-align">Id number</th>
                          <th class="center-align">Action</th>
                      </tr>
                  </tfoot>
               
                  <tbody>
                    @foreach($pendingStudents as $key => $student)
                      <tr>
                      <td class="center-align">{{ $key+1 }}</td>
                      <td class="center-align">{{ ucwords($student->first_name .' '.$student->last_name ) }}</td>
                      <td class="center-align">{{ $student->email_address }}</td>
                      <td class="center-align">{{ $student->phone_number }}</td>
                        <td class="center-align">{{ $student->id_number }}</td>
                        <td class="center-align">
                            <a href="{{ route('students.show', $student->code) }}" title="view student details" class="btn-floating waves-effect waves-light btn-small green">
                                <i class="mdi-action-visibility"></i>
                              </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
          </div>
        </div>
      </div>
    </div>
    <!--/ profile-page-wall-share -->
  </div>
  <!--/ profile-page-wall -->
</div>
</section>
@endsection
@section('css')
    <!-- Custome CSS-->    
    <link href="/assets/css/custom-style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/assets/js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
@endsection
@section('js')
<script type="text/javascript" src="/assets/js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
<script>
    $(function(){
        $('.data-table').DataTable();
    })
</script>
@endsection
