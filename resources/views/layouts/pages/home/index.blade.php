@extends('layouts.app')
@section('body')
<section id="content">
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper" class=" grey lighten-3">
            <div class="container">
              <div class="row">
                <div class="col s12 m12 l12">
                  <h5 class="breadcrumbs-title">Shanyvin Investments Limited</h5>
                  <ol class="breadcrumb">
                      <li class="active">Welcome to ShanyVin Academy and Loan management portal</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!--breadcrumbs end-->
        <!--start container-->
        <div class="container">
            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!--card stats start-->
            <div id="card-stats">
                <div class="row">
                    <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <img src="/assets/images/logo.jpg" alt="" style="width:300px; length:350px;">
                        </div>
                    </div>
                </div>
                </div>
                <div class="row">
                    <div class="col s12 m4 l4">
                        <div class="card">
                            <div class="card-content  green white-text">
                                <p class="card-stats-title"><i class="mdi-social-group-add large"></i> </p>
                                <h4 class="card-stats-number">Our Clients</h4>
                            </div>
                            <div class="card-action  green darken-1 center">
                                <a class="btn btn-small waves-light yellow black-text" href="{{ route('clients.index') }}"><i class="mdi-action-account-child"></i> View</a>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4 l4">
                        <div class="card">
                            <div class="card-content light-green white-text">
                                <p class="card-stats-title"><i class="mdi-editor-attach-money large"></i></p>
                                <h4 class="card-stats-number">Loan applications</h4>
                            </div>
                            <div class="card-action light-green center">
                            <a  href="{{ route('loans.index') }}" class="btn waves-effect waves-light black btn-small white-text"><i class="mdi-social-share"></i> View</a>
                            </div>
                        </div>
                    </div>                           
                    <div class="col s12 m4 l4">
                        <div class="card">
                            <div class="card-content blue white-text">
                                <p class="card-stats-title"><i class="mdi-social-school large"></i></p>
                                <h4 class="card-stats-number">Forex academy</h4>
                                </p>
                            </div>
                            <div class="card-action blue darken-2 center">
                            <a href="{{ route('academy.index') }}" class="btn waves-effect waves-light yellow darken-4 btn-small white-text"><i class="mdi-editor-border-color"></i> View</a>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            <!--card stats end-->

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!--work collections start-->
            <div id="work-collections">
                <div class="row">
                    <div class="col s12 m12 l6">
                        <ul id="projects-collection" class="collection">
                            <li class="collection-item avatar">
                                <i class="mdi-file-folder circle light-blue darken-2"></i>
                                <span class="collection-header">Loans Types</span>
                                <p>Loans offered by ShanyVin Limited</p>
                                <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a>
                            </li>
                            @foreach($loanTypes as $loanType)
                            <li class="collection-item">
                                <div class="row">
                                    <div class="col s4">
                                    <p class="collections-title">{{ $loanType->name }}</p>
                                        
                                    </div>
                                    <div class="col s8">
                                            <p class="collections-content">{{ $loanType->description }}</p>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col s12 m12 l6">
                        <ul id="issues-collection" class="collection">
                            <li class="collection-item avatar">
                                <i class="mdi-action-bug-report circle red darken-2"></i>
                                <span class="collection-header">Academy</span>
                                <p>Courses Offered In Forex Academy</p>
                                <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a>
                            </li>
                            @foreach($courses as $course)
                            <li class="collection-item">
                                <div class="row">
                                    <div class="col s4">
                                        <p class="collections-title"><strong>{{ $course->name }}</strong></p>
                                        <p class="collections-content">{{ $course->description }}</p>
                                    </div>
                                    <div class="col s6">
                                    <span class="task-cat pink accent-2">KES {{ $course->cost }}</span>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!--work collections end-->

        </div>
        <!--end container-->
    </section>
@endsection
@section('css')
<style>
    .large {
        font-size: 35px !important;
    }
</style>
@endsection