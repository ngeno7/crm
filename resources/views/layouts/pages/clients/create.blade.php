@extends('layouts.app')

@section('body')
<section id="content">
        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper" class=" grey lighten-3">
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Clients</h5>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
            <li><a href="{{ route('clients.index') }}">Clients</a></li>
                <li class="active">Add Client</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->
<div class="container">
    <div class="card-panel">
        <div class="card-title">
            <h5 class="left-align">Add client</h5>
        </div>
        <div class="s8 offset-s2">
            <div class="row">
                <form class="col s12" method="post" action="{{ route('clients.store') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="input-field col s4">
                            <input id="first_name" name="first_name" type="text" required value="{{ old('last_name') }}">
                            <label for="first_name">First Name</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('first_name') ? $errors->first('first_name') : ''}}
                            </span>
                        </div>
                        <div class="input-field col s4">
                            <input id="last_name" name="last_name" type="text" required value="{{ old('last_name') }}">
                            <label for="last_name">Last Name</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('last_name') ? $errors->first('last_name') : ''}}
                            </span>
                        </div>
                        <div class="input-field col s4">
                            <input id="middle_name" name="middle_name" type="text" value="{{ old('middle_name') }}">
                            <label for="middle_name">Middle Name</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('middle_name') ? $errors->first('middle_name') : ''}}
                            </span>
                        </div>
                     </div>
                     <div class="row">
                            <div class="input-field col s6">
                                <input id="id_number" name="id_number" type="number" value="{{ old('id_number') }}">
                                <label for="id_number">ID Number</label>
                                <span class="helper-text" data-error="wrong">
                                        {{ $errors->has('id_number') ? $errors->first('id_number') : ''}}
                                </span>
                            </div>
                            <div class="input-field col s6">
                                <input id="pin_number" name="pin_number" type="text" required value="{{ old('pin_number') }}">
                                <label for="pin_number">PIN Number</label>
                                <span class="helper-text" data-error="wrong">
                                        {{ $errors->has('pin_number') ? $errors->first('pin_number') : ''}}
                                </span>
                            </div>
                     </div>
                    <div class="row">
                        <div class="input-field col s4">
                            <input id="email" name="email_address" type="text" value="{{ old('email_address') }}">
                            <label for="email_address">Email</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('email_address') ? $errors->first('email_address') : ''}}
                            </span>
                        </div>
                        <div class="input-field col s4">
                            <input id="phone_number" name="phone_number" type="number" value="{{ old('phone_number') }}" required>
                            <label for="phone_number">Phone Number</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('phone_number') ? $errors->first('phone_number') : ''}}
                            </span>
                        </div>
                        <div class="input-field col s4">
                            <input id="address" name="address" type="text" value="{{ old('address') }}">
                            <label for="address">Address</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('address') ? $errors->first('address') : ''}}
                            </span>
                        </div>
                    </div>
                    <div class="row">
                            <div class="input-field col s4">
                                    <input id="company" name="company" type="text" value="{{ old('company') }}">
                                    <label for="company">Company</label>
                                    <span class="helper-text" data-error="wrong">
                                            {{ $errors->has('company') ? $errors->first('company') : ''}}
                                    </span>
                            </div>
                            <div class="input-field col s4">
                                <input id="guarantor_name" name="guarantor_name" type="text" value="{{ old('guarantor_name') }}">
                                <label for="guarantor_name">Guarantor Name</label>
                                <span class="helper-text" data-error="wrong">
                                        {{ $errors->has('guarantor_name') ? $errors->first('guarantor_name') : ''}}
                                </span>
                            </div>
                            <div class="input-field col s4">
                                <input id="guarantor_id_number" name="guarantor_id_number" type="number" value="{{ old('guarantor_id_number') }}">
                                <label for="guarantor_id_number">Guarantor ID Number</label>
                                <span class="helper-text" data-error="wrong">
                                        {{ $errors->has('guarantor_id_number') ? $errors->first('guarantor_id_number') : ''}}
                                </span>
                            </div>
                            <div class="input-field col s4">
                            <input id="guarantor_email_address" name="guarantor_email_address" type="email" value="{{ old('guarantor_email_address') }}">
                                <label for="guarantor_email_address">Guarantor Email Address</label>
                                <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('guarantor_email_address') ? $errors->first('guarantor_email_address') : ''}}
                                </span>
                            </div>
                    </div>
                    <div class="row">
                            <div class="input-field col s6">
                                <input id="guarantor_phone_number" name="guarantor_phone_number" type="text" value="{{ old('guarantor_phone_number') }}">
                                <label for="guarantor_phone_number">Guarantor Phone Number</label>
                                <span class="helper-text" data-error="wrong">
                                        {{ $errors->has('guarantor_phone_number') ? $errors->first('guarantor_phone_number') : ''}}
                                </span>
                            </div>
                            <div class="input-field col s6">
                                <input id="guarantor_address" name="guarantor_address" type="text"  value="{{ old('guarantor_address') }}">
                                <label for="guarantor_address">Guarantor Address</label>
                                <span class="helper-text" data-error="wrong">
                                        {{ $errors->has('guarantor_address') ? $errors->first('guarantor_address') : ''}}
                                </span>
                            </div>
                    </div>
                      <div class="row">
                            <div class="input-field col s12">
                               <button class="btn cyan waves-effect waves-light right" type="submit">Save
                                    <i class="mdi-action-done-all left"></i>
                                </button>
                            </div>
                     </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
@endsection