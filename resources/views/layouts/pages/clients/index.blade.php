@extends('layouts.app')

@section('body')
<section id="content">
        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper" class=" grey lighten-3">
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Clients</h5>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">List of clients</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->
<div class="container">
    <div class="card-panel">
        <div class="card-title">
        <a href="{{ route('clients.create') }}" class="waves-effect waves-light btn-small green  btn right"><i class="mdi-content-add left"></i>Client</a>
        </div><br><br>
        <!-- profile-page-wall -->
 <div id="profile-page-wall" class="col s12 m12 l12">
    <!-- profile-page-wall-share -->
    <div id="profile-page-wall-share" class="row">
      <div class="col s12">
        <ul class="tabs tab-profile z-depth-1 light-green">
          <li class="tab col s3"><a class="white-text waves-effect waves-light active" href="#UpdateStatus"><i class="mdi-action-swap-horiz medium"></i>Accounts Pending</a>
          </li>
          <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#AddPhotos"><i class="mdi-action-swap-vert-circle medium"></i>Accounts Active</a>
          </li>
          <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#CreateAlbum"><i class="mdi-action-view-list medium"></i>Accounts Rejected</a>
          </li>                      
        </ul>
        <!-- UpdateStatus-->
        <div id="UpdateStatus" class="tab-content col s12  grey lighten-4">
            <div class="card-panel">
                <table class="data-table responsive-table">
                  <thead>
                      <tr>
                            <th class="center-align">No.</th>
                            <th class="center-align">Name</th>
                            <th class="center-align">Email</th>
                            <th class="center-align">Phone Number</th>
                            <th class="center-align">Company</th>
                            <th class="center-align">Date Of Reg</th>
                            <th class="center-align">Action</th>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                            <th class="center-align">No.</th>
                          <th class="center-align">Name</th>
                          <th class="center-align">Email</th>
                          <th class="center-align">Phone Number</th>
                          <th class="center-align">Company</th>
                          <th class="center-align">Date Of Reg</th>
                          <th class="center-align">Action</th>
                      </tr>
                  </tfoot>
               
                  <tbody>
                      @foreach($pendingClients as $key => $client)
                      <tr>
                      <td class="center-align">{{ $key+1 }}</td>
                      <td class="center-align">{{ ucwords($client->first_name." ".$client->last_name) }}</td>
                          <td class="center-align">{{ $client->email_address ? $client->email_address : '-' }}</td>
                          <td class="center-align">{{ $client->phone_number ? $client->phone_number : '-' }}</td>
                          <td class="center-align">{{ $client->company ? $client->company : '-' }}</td>
                      <td class="center-align"> {{ \Carbon\Carbon::parse($client->created_at)->format('Y-M-d, D')}}</td>
                          <td class="center-align">
                          <a href="{{ route('clients.edit', $client->code)}}" title="edit client" class="btn-floating waves-effect waves-light btn-small blue">
                            <i class="mdi-content-create"></i>
                          </a>
                          <ul id="dropdown{{$client->id}}" class="dropdown-content">
                                <li>
                                    <form action="{{ route('clients.update-status', $client->code) }}" method="post">
                                        @csrf
                                        {{ method_field('put') }}
                                        <input type="hidden" name="status" value="{{ \Repo\Helpers\EnumConsts::STATUS_active }}">
                                        <button type="submit" title="activate" class="btn-floating blue"><i class="mdi-action-done"></i></button>
                                    </form>
                                </li>
                                <li>
                                    <form action="{{ route('clients.update-status', $client->code) }}" method="post">
                                        @csrf
                                        {{ method_field('put') }}
                                        <input type="hidden" name="status" value="{{ \Repo\Helpers\EnumConsts::STATUS_rejected }}">
                                        <button type="submit" title="reject" class="btn-floating red"><i class="mdi-content-clear"></i></button>
                                    </form>
                                </li>
                              </ul>
                            <a class="btn dropdown-button btn-floating waves-effect waves-light" title="approve/disapprove" data-activates="dropdown{{$client->id}}"><i class="mdi-navigation-arrow-drop-down right"></i></a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
          </div>
        </div>
        <!-- AddPhotos -->
        <div id="AddPhotos" class="tab-content col s12  grey lighten-4">
          <div class="row">
                <div class="card-panel">
                        <table class="data-table responsive-table">
                          <thead>
                              <tr>
                                    <th class="center-align">No.</th>
                                    <th class="center-align">Name</th>
                                    <th class="center-align">Email</th>
                                    <th class="center-align">Phone Number</th>
                                    <th class="center-align">Company</th>
                                    <th class="center-align">Date Of Reg</th>
                                    <th class="center-align">Action</th>
                              </tr>
                          </thead>
                       
                          <tfoot>
                              <tr>
                                    <th class="center-align">No.</th>
                                  <th class="center-align">Name</th>
                                  <th class="center-align">Email</th>
                                  <th class="center-align">Phone Number</th>
                                  <th class="center-align">Company</th>
                                  <th class="center-align">Date Of Reg</th>
                                  <th class="center-align">Action</th>
                              </tr>
                          </tfoot>
                       
                          <tbody>
                              @foreach($activeClients as $key => $client)
                              <tr>
                              <td class="center-align">{{ $key+1 }}</td>
                              <td class="center-align">{{ ucwords($client->first_name." ".$client->last_name) }}</td>
                                  <td class="center-align">{{ $client->email_address ? $client->email_address : '-' }}</td>
                                  <td class="center-align">{{ $client->phone_number ? $client->phone_number : '-' }}</td>
                                  <td class="center-align">{{ $client->company ? $client->company : '-' }}</td>
                              <td class="center-align"> {{ \Carbon\Carbon::parse($client->created_at)->format('Y-M-d, D') }}</td>
                                  <td class="center-align">
                                  <a href="{{ route('clients.edit', $client->code)}}" title="edit client" class="btn-floating waves-effect waves-light btn-small blue">
                                    <i class="mdi-content-create"></i>
                                  </a>
                                  <ul id="dropdown{{$client->id}}" class="dropdown-content">
                                        <li>
                                            <form action="{{ route('clients.update-status', $client->code) }}" method="post">
                                                @csrf
                                                {{ method_field('put') }}
                                                <input type="hidden" name="status" value="{{ \Repo\Helpers\EnumConsts::STATUS_rejected }}">
                                                <button type="submit" title="reject" class="btn-floating red"><i class="mdi-content-clear"></i></button>
                                            </form>
                                        </li>
                                      </ul>
                                    <a class="btn dropdown-button btn-floating waves-effect waves-light" title="approve/disapprove" data-activates="dropdown{{ $client->id }}">
                                        <i class="mdi-navigation-arrow-drop-down right"></i>
                                    </a>                     
                                  </td>
                              </tr>
                              @endforeach
                          </tbody>
                        </table>
                  </div>
          </div>
        </div>
        <!-- CreateAlbum -->
        <div id="CreateAlbum" class="tab-content col s12  grey lighten-4">
          <div class="row">
                <div class="card-panel">
                        <table class="data-table responsive-table">
                          <thead>
                              <tr>
                                    <th class="center-align">No.</th>
                                    <th class="center-align">Name</th>
                                    <th class="center-align">Email</th>
                                    <th class="center-align">Phone Number</th>
                                    <th class="center-align">Company</th>
                                    <th class="center-align">Date Of Reg</th>
                                    <th class="center-align">Action</th>
                              </tr>
                          </thead>
                       
                          <tfoot>
                              <tr>
                                    <th class="center-align">No.</th>
                                  <th class="center-align">Name</th>
                                  <th class="center-align">Email</th>
                                  <th class="center-align">Phone Number</th>
                                  <th class="center-align">Company</th>
                                  <th class="center-align">Date Of Reg</th>
                                  <th class="center-align">Action</th>
                              </tr>
                          </tfoot>
                       
                          <tbody>
                              @foreach($rejectedClients as $key => $client)
                              <tr>
                              <td class="center-align">{{ $key+1 }}</td>
                              <td class="center-align">{{ ucwords($client->first_name." ".$client->last_name) }}</td>
                                  <td class="center-align">{{ $client->email_address ? $client->email_address : '-' }}</td>
                                  <td class="center-align">{{ $client->phone_number ? $client->phone_number : '-' }}</td>
                                  <td class="center-align">{{ $client->company ? $client->company : '-' }}</td>
                              <td class="center-align"> {{ \Carbon\Carbon::parse($client->created_at)->format('Y-M-d, D')}}</td>
                                  <td class="center-align">
                                  <a href="{{ route('clients.edit', $client->code)}}" title="edit client" class="btn-floating waves-effect waves-light btn-small blue">
                                    <i class="mdi-content-create"></i>
                                  </a>
                                  <ul id="dropdown{{$client->id}}" class="dropdown-content">
                                        <li>
                                            <form action="{{ route('clients.update-status', $client->code) }}" method="post">
                                                @csrf
                                                {{ method_field('put') }}
                                                <input type="hidden" name="status" value="{{ \Repo\Helpers\EnumConsts::STATUS_active }}">
                                                <button type="submit" title="activate" class="btn-floating green"><i class="mdi-action-done"></i></button>
                                            </form>
                                        </li>
                                      </ul>
                                    <a class="btn dropdown-button btn-floating waves-effect waves-light" title="approve/disapprove" data-activates="dropdown{{ $client->id }}">
                                        <i class="mdi-navigation-arrow-drop-down right"></i>
                                    </a> 
                                  </td>
                              </tr>
                              @endforeach
                          </tbody>
                        </table>
                  </div>
          </div>
        </div>
      </div>
    </div>
    <!--/ profile-page-wall-share -->
  </div>
  <!--/ profile-page-wall -->
</div>
</section>
@endsection
@section('css')
    <!-- Custome CSS-->    
    <link href="/assets/css/custom-style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="assets/js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
@endsection
@section('js')
<script type="text/javascript" src="/assets/js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
<script>
    $(function(){
        $('.data-table').DataTable();
    })
</script>
@endsection
