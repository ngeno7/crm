@extends('layouts.app')

@section('body')
<section id="content">
        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper" class=" grey lighten-3">
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Loans Applications</h5>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">List of applications Loans</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->
<div class="container">
    <div class="card-panel">
        <div class="card-title">
                <a href="{{ route('loans.create') }}" class="waves-effect waves-light btn-small green  btn right"><i class="mdi-content-add left"></i> Loan</a>
                </div><br><br>
        <div id="profile-page-wall" class="col s12 m9">
                <!-- profile-page-wall-share -->
                <div id="profile-page-wall-share" class="row">
                  <div class="col s12">
                    <ul class="tabs tab-profile z-depth-1 light-green">
                      <li class="tab col s3"><a class="white-text waves-effect waves-light active" href="#UpdateStatus"><i class="mdi-action-swap-horiz medium"></i>Pending Loans</a>
                      </li>
                      <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#AddPhotos"><i class="mdi-action-swap-vert-circle medium"></i>Active Loans</a>
                      </li>
                      <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#CreateAlbum"><i class="mdi-action-view-list medium"></i>Disapproved Loans</a>
                      </li>                      
                    </ul>
                    <!-- UpdateStatus-->
                    <div id="UpdateStatus" class="tab-content col s12  grey lighten-4">
                            <div class="card-panel">
                                                <table class="data-table responsive-table">
                                                  <thead>
                                                      <tr>
                                                            <th class="center-align">No.</th>
                                                            <th class="center-align">Name</th>
                                                            <th class="center-align">Membership no</th>
                                                            <th class="center-align">Loan type</th>
                                                            <th class="center-align">Loan (KES)</th>
                                                            <th class="center-align">Amount to repay (KES)</th>
                                                            <th class="center-align">Date of disbursement</th>
                                                            <th class="center-align">End of term</th>
                                                            <th class="center-align">Action</th>
                                                      </tr>
                                                  </thead>
                                                  <tfoot>
                                                      <tr>
                                                        <th class="center-align">No.</th>
                                                        <th class="center-align">Name</th>
                                                        <th class="center-align">Membership no</th>
                                                        <th class="center-align">Loan type</th>
                                                        <th class="center-align">Loan (KES)</th>
                                                        <th class="center-align">Amnt to repay (KES)</th>
                                                        <th class="center-align">Date of disbursement</th>
                                                        <th class="center-align">End of term</th>
                                                        <th class="center-align">Action</th>
                                                      </tr>
                                                  </tfoot>
                                                  <tbody>
                                                      @foreach($pendingLoans as $key=>$loan)
                                                      <tr>
                                                        <td class="center-align">{{ $key+1 }}</td>
                                                        <td>{{ ucwords($loan->client->first_name .' '.$loan->client->last_name) }}</td>
                                                        <td>{{ $loan->client->membership_number }}</td>
                                                        <td>{{ $loan->loanType->name }}</td>
                                                        <td>{{ number_format($loan->amount_disbursed, 2) }}</td>
                                                        <td>
                                                          {{ number_format(((($loan->loanType->interest/100*$loan->loan_period)*$loan->amount_disbursed) + $loan->amount_disbursed),2) }}
                                                        </td>
                                                        <td>{{ \Carbon\Carbon::parse($loan->date_of_disbursement)->format('Y-M-d')}}</td>
                                                        <td>{{ 
                                                                \Repo\Helpers\EnumConsts::LOAN_PERIOD_week == $loan->period_type
                                                                   ? \Carbon\ Carbon::parse($loan->date_of_disbursement)->addWeeks($loan->loan_period)->format('Y-M-d') 
                                                                   : \Carbon\ Carbon::parse($loan->date_of_disbursement)->addMonths($loan->loan_period)->format('Y-M-d') 
                                                                   }}
                                                        </td>
                                                        <td class="center-align">
                                                            <a href="{{ route('loans.show', $loan->code) }}" title="view loans details" class="btn-floating waves-effect waves-light btn-small orange">
                                                              <i class="mdi-action-visibility"></i>
                                                            </a>
                                                            <a href="" class="btn-floating waves-effect waves-light btn-small green"><i class="mdi-editor-attach-file"></i></a>
                                                       </td>
                                                        </tr>
                                                      @endforeach
                                                  </tbody>
                                                </table>
                            
                            </div>
                    </div>
                    <!-- AddPhotos -->
                    <div id="AddPhotos" class="tab-content col s12  grey lighten-4">
                      <div class="row">
                            <div class="card-panel">
                                                <table class="data-table responsive-table">
                                                  <tbody>
                                                      <?php $total = 0; $loanTotal = 0?>
                                                      @foreach($activeLoans as $key=>$loan)
                                                      <?php
                                                      	$total += (($loan->loanType->interest/100*$loan->loan_period)*$loan->amount_disbursed) + $loan->amount_disbursed;
                                                      	$loanTotal += $loan->amount_disbursed;
                                                       ?>
                                                      <tr>
                                                        <td class="center-align">{{ $key+1 }}</td>
                                                        <td>{{ ucwords($loan->client->first_name .' '.$loan->client->last_name) }}</td>
                                                        <td>{{ $loan->client->membership_number }}</td>
                                                        <td>{{ $loan->loanType->name }}</td>
                                                        <td>{{ number_format($loan->amount_disbursed, 2) }}</td>
                                                        <td>{{ number_format(((($loan->loanType->interest/100*$loan->loan_period)*$loan->amount_disbursed) + $loan->amount_disbursed),2) }}</td>
                                                        <td>{{ \Carbon\Carbon::parse($loan->date_of_disbursement)->format('Y-M-d')}}</td>
                                                        <td>{{ 
                                                          \Repo\Helpers\EnumConsts::LOAN_PERIOD_week == $loan->period_type
                                                             ? \Carbon\ Carbon::parse($loan->date_of_disbursement)->addWeeks($loan->loan_period)->format('Y-M-d') 
                                                             : \Carbon\ Carbon::parse($loan->date_of_disbursement)->addMonths($loan->loan_period)->format('Y-M-d') 
                                                             }}
                                                        </td>
                                                        <td class="center-align">
                                                            <a href="{{ route('loans.show', $loan->code) }}" title="view loans details" class="btn-floating waves-effect waves-light btn-small orange">
                                                              <i class="mdi-action-visibility"></i>
                                                            </a>
                                                          <a href="{{ route('loan-statement', $loan->code) }}" class="btn-floating waves-effect waves-light btn-small green" title="download loan statement"><i class="mdi-editor-attach-file"></i></a>
                                                       </td>
                                                        </tr>
                                                      @endforeach
                                                  </tbody>
                                                  <thead>
                                                      <tr>
                                                        <th class="center-align">No.</th>
                                                        <th class="center-align">Name</th>
                                                        <th class="center-align">Membership no</th>
                                                        <th class="center-align">Loan type</th>
                                                        <th class="center-align">Loan (KES){{ number_format($loanTotal, 2) }}</th>
                                                        <th class="center-align">Amount to repay (KES) {{ number_format($total, 2) }}</th>
                                                        <th class="center-align">Date of disbursement</th>
                                                        <th class="center-align">End of term</th>
                                                        <th class="center-align">Action</th>
                                                      </tr>
                                                  </thead>
                                                  <tfoot>
                                                      <tr>
                                                        <th class="center-align">No.</th>
                                                        <th class="center-align">Name</th>
                                                        <th class="center-align">Membership no</th>
                                                        <th class="center-align">Loan type</th>
                                                        <th class="center-align">Loan (KES){{ number_format($loanTotal, 2) }}</th>
                                                        <th class="center-align">Amount to repay (KES) {{ number_format($total, 2) }}</th>
                                                        <th class="center-align">Date of disbursement</th>
                                                        <th class="center-align">End of term</th>
                                                        <th class="center-align">Action</th>
                                                      </tr>
                                                  </tfoot>
                                                </table>
                            
                            </div>
                      </div>
                    </div>
                    <!-- CreateAlbum -->
                    <div id="CreateAlbum" class="tab-content col s12  grey lighten-4">
                      <div class="row">
                            <div class="card-panel">
                                                <table class="data-table responsive-table">
                                                  <thead>
                                                      <tr>
                                                            <th class="center-align">No.</th>
                                                            <th class="center-align">Name</th>
                                                            <th class="center-align">Membership no</th>
                                                            <th class="center-align">Loan</th>
                                                            <th class="center-align">Loan (KES)</th>
                                                            <th class="center-align">Amnt repay (KES)</th>
                                                            <th class="center-align">Date of disbursement</th>
                                                            <th class="center-align">End of term</th>
                                                            <th class="center-align">Action</th>
                                                      </tr>
                                                  </thead>
                                               
                                                  <tfoot>
                                                      <tr>
                                                        <th class="center-align">No.</th>
                                                        <th class="center-align">Name</th>
                                                        <th class="center-align">Membership no</th>
                                                        <th class="center-align">Loan type</th>
                                                        <th class="center-align">Loan (KES)</th>
                                                        <th class="center-align">Amount to repay (KES)</th>
                                                        <th class="center-align">Date of disbursement</th>
                                                        <th class="center-align">End of term</th>
                                                        <th class="center-align">Action</th>
                                                      </tr>
                                                  </tfoot>
                                                  <tbody>
                                                      @foreach($rejectedLoans as $key=>$loan)
                                                      <tr>
                                                        <td class="center-align">{{ $key+1 }}</td>
                                                        <td>{{ ucwords($loan->client->first_name .' '.$loan->client->last_name) }}</td>
                                                        <td>{{ $loan->client->membership_number }}</td>
                                                        <td>{{ $loan->loanType->name }}</td>
                                                        <td>{{ number_format($loan->amount_disbursed, 2) }}</td>
                                                        <td>{{ number_format(((($loan->loanType->interest/100*$loan->loan_period)*$loan->amount_disbursed) + $loan->amount_disbursed),2) }}</td>
                                                        <td>{{ \Carbon\Carbon::parse($loan->date_of_disbursement)->format('Y-M-d')}}</td>
                                                        <td>{{ 
                                                          \Repo\Helpers\EnumConsts::LOAN_PERIOD_week == $loan->period_type
                                                             ? \Carbon\ Carbon::parse($loan->date_of_disbursement)->addWeeks($loan->loan_period)->format('Y-M-d') 
                                                             : \Carbon\ Carbon::parse($loan->date_of_disbursement)->addMonths($loan->loan_period)->format('Y-M-d') 
                                                             }}
                                                        </td>
                                                        <td class="center-align">
                                                            <a href="{{ route('loans.show', $loan->code) }}" title="view loans details" class="btn-floating waves-effect waves-light btn-small orange">
                                                              <i class="mdi-action-visibility"></i>
                                                            </a>
                                                       </td>
                                                        </tr>
                                                      @endforeach
                                                  </tbody>
                                                </table>
                            
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!--/ profile-page-wall-share -->
              </div>
            </div>
</section>
@endsection
@section('css')
    <!-- Custome CSS-->    
    <link href="/assets/css/custom-style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="assets/js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
@endsection
@section('js')
<script type="text/javascript" src="/assets/js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
<script>
    $(function(){
        $('.data-table').DataTable();
    })
</script>
@endsection