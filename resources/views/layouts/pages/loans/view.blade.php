@extends('layouts.app')

@section('body')
<section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper" class=" grey lighten-3">
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Loan Details</h5>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                <li><a href="{{ route('loans.index') }}">Loans applications</a></li>
                    <li class="active">Loan Details</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
          <div class="row">
              <div class="col s12 m4">
                <ul id="profile-page-about-details" class="collection z-depth-1">
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s6 grey-text darken-1"><i class="mdi-action-account-circle"></i> Name</div>
                      <div class="col s6 grey-text text-darken-4 right-align">{{ ucwords($loan->client->first_name .' '.$loan->client->last_name) }}</div>
                    </div>
                  </li>
                  <li class="collection-item">
                      <div class="row">
                        <div class="col s6 grey-text darken-1"><i class="mdi-action-assignment-ind"></i> Membership number</div>
                        <div class="col s6 grey-text text-darken-4 right-align">{{ $loan->client->membership_number }}</div>
                      </div>
                    </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s6 grey-text darken-1"><i class="mdi-action-settings-phone"></i> Phone number</div>
                      <div class="col s6 grey-text text-darken-4 right-align">{{ $loan->client->phone_number }}</div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s6 grey-text darken-1"><i class="mdi-action-speaker-notes"></i>Email address</div>
                      <div class="col s6 grey-text text-darken-4 right-align">{{ $loan->client->email_address }}</div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s6 grey-text darken-1"><i class="mdi-file-folder-open"></i> ID number</div>
                      <div class="col s6 grey-text text-darken-4 right-align">{{ $loan->client->id_number }}</div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s6 grey-text darken-1"><i class="mdi-av-subtitles"></i> PIN number</div>
                      <div class="col s6 grey-text text-darken-4 right-align">{{ $loan->client->pin_number }}</div>
                    </div>
                  </li>
                  <li class="collection-item">
                    <div class="row">
                      <div class="col s6 grey-text darken-1"><i class="mdi-action-account-balance-wallet"></i> Company</div>
                      <div class="col s6 grey-text text-darken-4 right-align">{{ $loan->client->company }}</div>
                    </div>
                  </li>
                </ul>
              </div>
              <!-- profile-page-wall -->
              <div id="profile-page-wall" class="col s12 m8">
                    <!-- profile-page-wall-share -->
                    <div id="profile-page-wall-share" class="row">
                      <div class="col s12">
                        <ul class="tabs tab-profile z-depth-1 light-green">
                          <li class="tab col s3"><a class="white-text waves-effect waves-light active" href="#UpdateStatus"><i class="mdi-action-swap-vert-circle medium"></i>Loan details</a>
                          </li>
                          <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#AddPhotos"><i class="mdi-action-swap-horiz medium"></i>New Repayment</a>
                          </li>
                          
                          <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#CreateAlbum"><i class="mdi-action-view-list medium"></i> Loan Repayment history</a>
                          </li>                      
                        </ul>
                        <!-- UpdateStatus-->
                        <div id="UpdateStatus" class="tab-content col s12  grey lighten-4">
                          <div class="row">
                             <div class="col s12 m12 l12">
                                  <ul id="issues-collection" class="collection">
                                      <li class="collection-item avatar">
                                          <i class="mdi-action-view-week circle red darken-2"></i>
                                          <span class="collection-header"><strong>Loan details</strong></span>
                                          <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a>
                                      </li>
                                      @if($loan->status == \Repo\Helpers\EnumConsts::STATUS_pending)
                                      <li class="collection-item">
                                      <form class="row" action="{{ route('loans.status', $loan->code) }}" method="POST">
                                        @csrf
                                        {{ method_field('put') }}
                                          <div class="input-field col s4">
                                              <input id="amount_disbursed" name="amount_disbursed" type="number" required>
                                              <label for="amount_disbursed">Amount</label>
                                              <span class="helper-text" data-error="wrong"></span>
                                            </div>
                                            <div class="col s12 m4 l4">
                                                <div class="input-field">
                                                    <select name="status" id="status">
                                                            <option disabled selected placeholder>select action...</option>
                                                            <option value="{{ \Repo\Helpers\EnumConsts::STATUS_active }}">Approve application</option>
                                                            <option value="{{ \Repo\Helpers\EnumConsts::STATUS_rejected }}" >Reject application</option>
                                                    </select>
                                                    <label for="status">Approve/Disapprove</label>
                                                </div>
                                            </div>
                                            <div class="col s12 m4 l4">
                                                <button class="btn green  waves-effect waves-light left" type="submit" style="margin-top:25px;">Approve/Disapprove
                                                    <i class="mdi-action-done-all left"></i>
                                                </button>
                                            </div>
                                          </form>
                                      </li>
                                    @endif
                                      <li class="collection-item">
                                          <div class="row">
                                              <div class="col s4">
                                                  <p class="collections-title"><strong>Loan</strong></p>
                                              <p class="collections-content">{{ $loan->loanType->name }}</p>
                                              </div>
                                              <div class="col s4">
                                                  <p class="collections-title"><strong>Interest</strong></p>
                                                  <p class="collections-content">{{ number_format($loan->loanType->interest) }} %</p>
                                              </div>
                                              <div class="col s4">
                                                  <p class="collections-title"><strong>Level</strong></p>
                                                  <p class="collections-content">{{ $loan->loan_level }}</p>
                                              </div>
                                          </div>
                                      </li>
                                      <li class="collection-item">
                                          <div class="row">
                                              <div class="col s4">
                                                  <p class="collections-title"><strong>Amount disbursed</strong></p>
                                              <p class="collections-content">KES {{ number_format($loan->amount_disbursed, 2) }}</p>
                                              </div>
                                              <div class="col s4">
                                                  <p class="collections-title"><strong>Date of disbursement</strong></p>
                                                  <p class="collections-content">{{ \Carbon\Carbon::parse($loan->date_of_disbursement)->format('Y-M-d, D')}} </p>
                                              </div>
                                              <div class="col s4">
                                                  <p class="collections-title"><strong>Loan term</strong></p>
                                                  <p class="collections-content">{{ $loan->loan_period }} {{ $loan->period_type == \Repo\Helpers\EnumConsts::LOAN_PERIOD_week ? 'Week(s)' : 'month(s)' }} </p>                                              
                                              </div>
                                          </div>
                                      </li>
                                      <li class="collection-item">
                                          <div class="row">
                                              <div class="col s6">
                                                  <p class="collections-title"><strong>Installment</strong></p>
                                                  <p class="collections-content">
                                                   KES {{ number_format($loan->installment_amount, 2) }} per {{ $loan->installment_type == \Repo\Helpers\EnumConsts::INSTALLMENT_TYPE_WEEKLY ? 'Week' : 'Month' }} 
                                                  </p>
                                              </div>
                                              <div class="col s6">                                                
                                                  <p class="collections-title"><strong>Amount to be paid</strong></p>
                                                  <p class="collections-content">
                                                     KES {{ number_format(((($loan->loanType->interest/100)*$loan->amount_disbursed * $loan->loan_period) + $loan->amount_disbursed),2) }}
                                                  </p>
                                              </div>
                                          </div>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                        </div>
                        <!-- AddPhotos -->
                        <div id="AddPhotos" class="tab-content col s12  grey lighten-4">
                          <div class="row">
                              <div class="card-panel">
                                <div class="card-title"><h6><strong>Add new repayment</strong></h6></div>
                                  <form action="{{ route('loan-repayments.store') }}" method="POST">
                                    @csrf
                                  <input type="hidden" name="loan_id" value="{{ $loan->id }}">
                                      <div class="row">
                                          <div class="input-field col s6">
                                            <input id="amount" name="amount" step="2" type="number" required>
                                            <label for="amount">Amount-KES</label>
                                          </div>
                                          <div class="input-field col s6">
                                            <select name="payment_mode" id="payment_mode" required>
                                                <option disabled selected placeholder>Select payment mode...</option>
                                                <option value="Bank">Bank</option>
                                                <option value="Mpesa">Mpesa</option>
                                                <option value="Cash deposit">Cash Deposit</option>
                                            </select>
                                          </div>
                                      </div>
                                      <div class="row">
                                          
                                          <div class="input-field col s6">
                                              <input id="comment" name="comment" type="text">
                                              <label for="comment">Comment</label>
                                            </div>
                                          <div class="input-field col s6">
                                              <input id="transaction_code" name="transaction_code" type="text">
                                              <label for="transaction_code">Transaction code</label>
                                          </div>
                                      </div>
                                      @if($loan->status == \Repo\Helpers\EnumConsts::STATUS_active)
                                      <div class="row">
                                          <div class="input-field col s12">
                                            <button class="btn green waves-effect waves-light right" type="submit">
                                                <i class="mdi-action-done-all"></i> Save
                                            </button>
                                        </div>
                                      </div>
                                      @else
                                      <div class="card-panel red lighten-2 center">The loan is not active</div>
                                      @endif
                                    </form>
                                  </div>
                          </div>
                        </div>
                        <!-- CreateAlbum -->
                        <div id="CreateAlbum" class="tab-content col s12  grey lighten-4">
                          <div class="row">
                            <div class="card-panel">
                            <table class="data-table responsive-table">
                              <thead>
                                <tr>
                                  <th>No.</th>
                                  <th>Transaction code</th>
                                  <th>Amount</th>
                                  <th>Payment mode</th>
                                  <th>Created at</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tfoot>
                                  <tr>
                                    <th>No.</th>
                                    <th>Transaction code</th>
                                    <th>Amount</th>
                                    <th>Payment mode</th>
                                    <th>Created at</th>
                                    <th>Action</th>
                                  </tr>
                              </tfoot>
                              <tbody>
                                  <?php $total = 0; ?>
                                  @foreach($loan->repayments as $key=>$repayment)
                                  <?php $total += $repayment->amount; ?>
                                  <tr>
                                    <td class="center-align">{{ $key+1 }}</td>
                                    <td>{{ ucwords($repayment->transaction_code) }}</td>
                                    <td>{{ number_format($repayment->amount, 2) }}</td>
                                    <td>{{ $repayment->payment_mode }}</td>
                                    <td>{{ \Carbon\Carbon::parse($repayment->created_at)->format('Y-m-d') }}</td>
                                    <td class="center-align">
                                    <a href="{{ route('loan-receipt', $repayment->code)}}" title="receipt" class="btn-floating waves-effect waves-light btn-small blue">
                                          <i class="mdi-action-visibility"></i>
                                        </a>
                                   </td>
                                    </tr>
                                  @endforeach
                                  <tr>
                                  <td colspan="2" class="center-align"><strong>Total installment - KES {{ number_format($total, 2) }}</strong></td>
                                  <td class="center-align">Amount Remaining {{ number_format((((($loan->loanType->interest/100)*($loan->amount_disbursed* $loan->loan_period)) + $loan->amount_disbursed) - $total), 2) }}</td>
                                  <td colspan="3" class="center-align"><strong>Total to be paid - KES {{ number_format(((($loan->loanType->interest/100)*$loan->amount_disbursed* $loan->loan_period) + $loan->amount_disbursed),2) }}</strong></td>
                                  </tr>
                              </tbody>
                            </table>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--/ profile-page-wall-share -->
                  </div>
                  <!--/ profile-page-wall -->
                </div>
 </section>
@endsection
@section('css')
<style>
  .medium {
    font-size: 25px !important;
  }
</style>
@endsection