@extends('layouts.app')
@section('css')
<style>
    .light-green {
        background-color : #ff9900 !important;
    }
</style>
@endsection
@section('body')
<section id="content">
        
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper" class=" grey lighten-3">
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Loan application</h5>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="{{ route('loans.index') }}">Loan applications</a></li>
                <li class="active">Loan application</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->
<div class="row">
    <div class="col s12">
    <div class="card-panel">
        <div class="card-title">
            <h5 class="left-align">Loan application</h5>
        </div>
            <div class="row">
                <form class="col s12" method="post" action="{{ route('loans.store') }}">
                    @csrf
                    <div class="row">
                        <div class="input-field col s4">
                            <select name="client_id" id="client_id">
                                <option disabled selected placeholder>Select client...</option>
                                @foreach($clients as $client)
                                    <option value="{{ $client->id }}">{{ $client->first_name .' '.$client->last_name }}</option>
                                @endforeach
                            </select>
                            <label for="client_id">Client</label>
                            <span class="helper-text" data-error="wrong">
                                    {{ $errors->has('client_id') ? $errors->first('client_id') : ''}}
                            </span>
                        </div>
                        <div class="input-field col s4">
                            <select name="loan_type_id" id="loan_type_id">
                                    <option disabled selected placeholder>Select loan type...</option>
                                @foreach($loanTypes as $loanType)
                                    <option value="{{ $loanType->id }}">{{ $loanType->name }}</option>
                                @endforeach
                            </select>
                            <label for="loan_type_id">Loan type</label>
                        </div>
                        <div class="input-field col s4">
                            <input id="amount_disbursed" name="amount_disbursed" type="number" required>
                            <label for="amount_disbursed">Loan Amount (KES)</label>
                            <span class="helper-text" data-error="wrong"></span>
                        </div>
                        <div class="input-field col s4">
                                <input id="loan_period" name="loan_period" type="number" required>
                                <label for="loan_period">Loan period</label>
                                <span class="helper-text" data-error="wrong"></span>
                        </div>
                        <div class="input-field col s4">
                            <select name="period_type" id="period_type" placeholder='select period type...' required>
                                    <option value="{{ \Repo\Helpers\EnumConsts::LOAN_PERIOD_month }}">Months(s)</option>
                                    <option value="{{ \Repo\Helpers\EnumConsts::LOAN_PERIOD_week }}">Weeks(s)</option>
                             </select>
                            <label for="installment_type">Period Type</label>
                            <span class="helper-text" data-error="wrong"></span>
                        </div>
                        <div class="input-field col s4">
                            <select name="installment_type" id="installment_type" placeholder='select installmant type...' required>
                                <option value="{{ \Repo\Helpers\EnumConsts::INSTALLMENT_TYPE_WEEKLY }}">Month</option>
                                <option value="{{ \Repo\Helpers\EnumConsts::INSTALLMENT_TYPE_MONTHLY }}">Week</option>
                            </select>
                            <label for="installment_type">Installment type</label>
                            <span class="helper-text" data-error="wrong"></span>
                        </div>
                        <div class="input-field col s4">
                            <input id="installment_amount" name="installment_amount" type="number" step="2" required value="">
                            <label for="installment_amount">Installment Amount</label>
                            <span class="helper-text" data-error="wrong"></span>
                        </div>
                        <div class="input-field col s4">
                            <select name="loan_level" id="loan_level" placeholder='select loan level...' required>
                                <option value="{{ \Repo\Helpers\EnumConsts::LEVEL_one }}">Level one</option>
                                <option value="{{ \Repo\Helpers\EnumConsts::LEVEL_two }}">Level two</option>
                                <option value="{{ \Repo\Helpers\EnumConsts::LEVEL_three }}">Level three</option>
                                <option value="{{ \Repo\Helpers\EnumConsts::LEVEL_four }}">Level four</option>
                                <option value="{{ \Repo\Helpers\EnumConsts::LEVEL_five }}">Level five</option>
                                <option value="{{ \Repo\Helpers\EnumConsts::LEVEL_six }}">Level six</option>
                            </select>
                            <label for="loan_level">Loan Level</label>
                            <span class="helper-text" data-error="wrong">
                                {{ $errors->has('loan_level') ? $errors->first('loan_level') : ''}}
                            </span>
                        </div>
                     </div>
                      <div class="row">
                            <div class="input-field col s12">
                               <button class="btn green waves-effect waves-light right" type="submit">Save <i class="mdi-action-done-all left"></i></button>
                            </div>
                     </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
@section('css')
<link rel="stylesheet" href="/css/app.css">
<link rel="stylesheet" href="/assets/js/plugins/select2/select2-materialize.css">
@endsection
@section('js')
<script src="/js/app.js"></script>
<script src="/js/loanCalculator.js"></script>
@endsection