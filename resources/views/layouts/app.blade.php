<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Shanvin Limited. ">
    <meta name="keywords" content="Shanvin Limited">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Shanyvin Investments Limited</title>

    <!-- Favicons-->
    {{-- <link rel="icon" href="/assets/images/favicon/favicon-32x32.png" sizes="32x32"> --}}
    <!-- Favicons-->
    {{-- <link rel="apple-touch-icon-precomposed" href="/assets/images/favicon/apple-touch-icon-152x152.png"> --}}
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    {{-- <meta name="msapplication-TileImage" content="/assets/images/favicon/mstile-144x144.png"> --}}
    <!-- For Windows Phone -->


    <!-- CORE CSS-->    
<link href="{{ asset('assets/css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
<link href="{{ asset('assets/css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->    
<link href="{{ asset('assets/css/custom-style.css') }}" type="text/css" rel="stylesheet" media="screen,projection">    
    <!-- CSS style Horizontal Nav (Layout 03)-->    
<link href="{{ asset('assets/css/style-horizontal.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
<link href="{{ asset('assets/js/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
<style>
    .light-green {
        background-color : #ff9900 !important;
    }
</style>
@yield('css')
</head>

<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="light-green">
                <div class="nav-wrapper">                    
                    
                    <ul class="left">
                      <li class="no-hover"><a href="#" data-activates="slide-out" class="menu-sidebar-collapse btn-floating btn-flat btn-medium waves-effect waves-light cyan hide-on-large-only"><i class="mdi-navigation-menu" ></i></a></li>
                      <li><h4 class="logo-wrapper"><a href="#" class="brand-logo darken-1"></a> </h4></li>
                    </ul>
                    <div class="header-search-wrapper hide-on-med-and-down">
                    </div>
                </div>
            </nav>

            <!-- HORIZONTL NAV START-->
             <nav id="horizontal-nav" class="white hide-on-med-and-down">
                <div class="nav-wrapper">                  
                  <ul id="nav-mobile" class="left hide-on-med-and-down">
                    <li>
                        <a href="/" class="cyan-text">
                            <i class="mdi-action-dashboard"></i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                    <a href="{{ route('clients.index') }}" class="cyan-text">
                            <i class="mdi-social-mood"></i>
                            <span>Clients</span>
                        </a>
                    </li>
                    <li>
                    <a href="{{ route('academy.index') }}" class="cyan-text">
                            <i class="mdi-social-school"></i>
                            <span>Academy</span>
                        </a>
                    </li>
                    <li>
                    <a href="{{ route('loan-repayments.index') }}" class="cyan-text">
                            <i class="mdi-editor-attach-money"></i>
                            <span>Loan repayments</span>
                        </a>
                    </li> 
                    <li>
                        <a href="{{ route('loans.index') }}" class="cyan-text">
                            <i class="mdi-social-share"></i>
                            <span>Loan Applications</span>
                        </a>
                    </li>
                  </ul>
                </div>
              </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav hide-on-large-only">
                <ul id="slide-out" class="side-nav leftside-navigation ">
                    <li class="user-details light-green darken-2">
                        <div class="row">
                            <div class="col col s4 m4 l4">
                                <img src="/assets/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                            </div>
                            <div class="col col s8 m8 l8">
                                <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">Admin<i class="mdi-navigation-arrow-drop-down right"></i></a>
                                <p class="user-roal">Administrator</p>
                            </div>
                        </div>
                    </li>
                    <li class="bold active"><a href="{{ route('clients.index') }}" class="waves-effect waves-cyan"><i class="mdi-social-mood"></i> Clients</a>
                    </li>
                    <li class="bold"><a href="{{ route('loan-types.index') }}" class="waves-effect waves-cyan"><i class="mdi-editor-attach-money"></i> Loans </a>
                    </li>
                    <li class="bold"><a href="{{ route('loans.index') }}" class="waves-effect waves-cyan"><i class="mdi-social-share"></i> Loan Disbursement</a>
                    </li>
                </ul>
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only yellow"><i class="mdi-navigation-menu"></i></a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->
            <!-- START CONTENT -->
            <div id="app">
                @yield('body')
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->

    <!-- START FOOTER -->
    <footer class="page-footer light-green">
        <div class="footer-copyright">
            <div class="container">
                Copyright © {{ \Carbon\Carbon::now()->format('Y') }}. All rights reserved.
                <span class="right"> Design and Developed by ShanyVin Limited.</span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


    <!-- ================================================
    Scripts
    ================================================ -->
    
    <!-- jQuery Library -->
    <script type="text/javascript" src="/assets/js/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="/assets/js/materialize.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="/assets/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- sparkline -->
    <script type="text/javascript" src="/assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/sparkline/sparkline-script.js"></script>  
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="/assets/js/plugins.js"></script>
    <!-- Toast Notification -->
    <script type="text/javascript">
    // Toast Notification
    </script>
    @yield('js')
</body>

</html>