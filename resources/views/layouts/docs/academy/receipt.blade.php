<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Loan receipt</title>
<link href="{{ public_path('/css/style.css') }}" rel="stylesheet">    
</head>
<body>
    <div class="container">
        <div class="row" id="app">
            <div class="col-md-12">
                <div style="margin-left:40%">
                <img src="{{ public_path('/assets/images/logo.jpg') }}" alt="" style="height:100px; width:200px;">
                </div>
            </div>
        </div>
        <div class="row">
                <div>
                    <address class="text-normal" style="margin-top:10px;">
                        ShanyVin Investments Limited,<br>
                        0746888278 <br>
                        info@sifafx.com <br>
                        www.sifafx.com 
                    </address>
                </div>
                    <div style="margin-left:80% !important; margin-top:-65px !important;">
                        <p class="text-normal">
                          <br>
                           <br>
                        </p>
                    </div>
        </div><br><hr>
            <div class="row margin-top-receipt-body border" style="padding:2px;">
                <div class="col-md-12">
                            <div class="row">
                            <div class="col-md-5">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td colspan="2" class="text-right">
                                            Date: {{ \Carbon\Carbon::parse($coursePayment->created_at)->format('Y-M-d') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-left test-cell-bg"> Student Details: {{ ucwords($coursePayment->studentCourse->student->first_name .' '.$coursePayment->studentCourse->student->last_name) }}</th>
                                        <th class="text-left test-cell-bg">
                                             
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-left">Payment Method</td>
                                        <td>{{ $coursePayment->payment_mode }}</td>
                                    </tr>
                                    <tr>
                                        <td>Transaction number</td>
                                        <td>{{ $coursePayment->transaction_code }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        </div><br><br>
                        <div class="row">
                                <div class="col-md-12">
                                    <table class="table">
                                        <thead>
                                            <tr class="test-cell-bg">
                                                <th>Item</th>
                                                <th>Description</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <td class="text-left text-normal">{{ ucwords($coursePayment->studentCourse->course->name) }}<br><br></td>
                                                <td class="text-left text-normal" rowspan="2">
                                                    {{ $coursePayment->comment }}
                                                </td>
                                            <td class="text-left text-normal">KES {{ number_format($coursePayment->amount, 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-right text-normal"><b>Total Amount:</b></td>
                                                <td class="text-left text-normal">KES {{ number_format($coursePayment->amount, 2) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center text-normal">Signature: ......................</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                </div>
                </div>
         <br>
            <div class="row">
                <div class="text-center">
                       <span class=" text-normal margin-right-1"> 
                        www.sifafx.com
                       </span>
                     <span class="text-normal margin-right-1">
                        info@sifafx.com
                    </span>
                     <span class="text-normal margin-right-1">
                        0746888278
                    </span>
                </div>    
            </div>       
        
    </div>
</body>
</html>