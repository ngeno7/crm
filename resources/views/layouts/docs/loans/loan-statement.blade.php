<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Loan statement</title>
<link href="{{ public_path('/css/style.css') }}" rel="stylesheet">    
</head>
<body>
    <div class="container">
        <div class="row" id="app">
            <div class="col-md-12">
                <div style="margin-left:40%">
                <img src="{{ public_path('/assets/images/logo.jpg') }}" alt="" style="height:100px; width:200px;">
                </div>
            </div>
        </div>
        <div class="row">
                <div>
                    <address class="text-normal" style="margin-top:10px;">
                        ShanyVin Investments Limited,<br>
                        0746888278 <br>
                        info@sifafx.com <br>
                        www.sifafx.com 
                    </address>
                </div>
                    <div style="margin-left:80% !important; margin-top:-65px !important;">
                        <p class="text-normal">
                          <br>
                           <br>
                        </p>
                    </div>
        </div><br><hr>
            <div class="row margin-top-receipt-body border" style="padding:2px;">
                <div class="col-md-12">
                    <h3 class="text-center">Loan Statement</h3>
                            <div class="row">
                            <div class="col-md-5">
                            <table class="table" style="width:100%">
                                <thead>
                                    <tr>
                                        <td class="text-left test-cell-bg">
                                            Principal
                                        </td>
                                        <td>
                                               KES {{ number_format($loan->amount_disbursed, 2) }}
                                        </td>
                                        <td class="text-left text-center test-cell-bg">
                                            Client name
                                        </td>
                                        <td>
                                            {{ ucwords($loan->client->first_name.' '.$loan->client->last_name) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left test-cell-bg"> Interest</td>
                                        <td class="text-left">{{ $loan->loanType->interest }} %</td>
                                        <td class="text-left text-center test-cell-bg"> Membership no</td>
                                        <td class="text-left">{{ $loan->client->membership_number }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left test-cell-bg"> Total payment</td>
                                        <td class="text-left"> KES {{ number_format(((($loan->loanType->interest/100)*$loan->amount_disbursed) + $loan->amount_disbursed),2) }}</td>
                                        <td class="text-left text-center test-cell-bg"> Phone no</td>
                                        <td>{{ $loan->client->phone_number }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left test-cell-bg"> Disbursement date</td>
                                        <td class="text-left">
                                                {{ \Carbon\Carbon::parse($loan->date_of_disbursement)->format('Y-M-d') }}
                                        </td>
                                        <td class="text-left text-center test-cell-bg"> Email</td>
                                        <td>{{ $loan->client->email_address }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left test-cell-bg"> End date</td>
                                        <td class="text-left">
                                                {{ 
                                                    \Repo\Helpers\EnumConsts::LOAN_PERIOD_week == $loan->period_type
                                                       ? \Carbon\ Carbon::parse($loan->date_of_disbursement)->addWeeks($loan->loan_period)->format('Y-M-d') 
                                                       : \Carbon\ Carbon::parse($loan->date_of_disbursement)->addMonths($loan->loan_period)->format('Y-M-d') 
                                                }}
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                            
                        </div>
                        </div><br><br>
                        <div class="row">
                                <div class="col-md-12">
                                    <table class="table">
                                        <thead>
                                            <tr class="test-cell-bg">
                                                <th>Loan</th>
                                                <th>Loan level</th>
                                                <th>Loan Installment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <td>{{ $loan->loanType->name }}</td>
                                            <td>{{ $loan->loan_level }}</td>
                                            <td>
                                                   KES {{ number_format($loan->installment_amount, 2) }} per {{ $loan->installment_type == \Repo\Helpers\EnumConsts::INSTALLMENT_TYPE_WEEKLY ? 'Week' : 'Month' }} 
                                            </td>
                                                
                                            </tr>
                                            <tr class="test-cell-bg">
                                                <th>Interest rate</th>
                                                <th>Loan term</th>
                                                <th>-</th>
                                            </tr>
                                            <tr>
                                                <td>{{ $loan->loanType->interest }} %</td>
                                                <td>{{ $loan->loan_period }} {{ \Repo\Helpers\EnumConsts::LOAN_PERIOD_week == $loan->period_type ? "Week(s)" : "month(s)" }}</td>
                                                <td>
                                                    -
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                </div>
                </div>
         <br>
            <div class="row">
                <div class="text-center">
                       <span class=" text-normal margin-right-1"> 
                        www.sifafx.com
                       </span>
                     <span class="text-normal margin-right-1">
                        info@sifafx.com
                    </span>
                     <span class="text-normal margin-right-1">
                        0746888278
                    </span>
                </div>    
            </div>       
        
    </div>
</body>
</html>