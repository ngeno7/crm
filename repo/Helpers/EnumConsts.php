<?php
namespace Repo\Helpers;

abstract class EnumConsts {
    //installment type
    const INSTALLMENT_TYPE_WEEKLY = 1;
    const INSTALLMENT_TYPE_MONTHLY = 2;

    //app statuses
    const STATUS_active = 3;
    const STATUS_pending = 4;
    const STATUS_rejected = 5;

    //period type loans
    const LOAN_PERIOD_week = 6;
    const LOAN_PERIOD_month = 7;

    const LEVEL_one = 'Level 1';
    const LEVEL_two = 'Level 2';
    const LEVEL_three = 'Level 3';
    const LEVEL_four = 'Level 4';
    const LEVEL_five = 'Level 5';
    const LEVEL_six = 'Level 6';
}