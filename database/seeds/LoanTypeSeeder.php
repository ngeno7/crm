<?php

use Illuminate\Database\Seeder;
use App\Models\LoanType;
use Carbon\Carbon;

class LoanTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        LoanType::insert([
            [
                'code' => time() + rand(1, 100000000),
                'name' => 'Jiinue loan',
                'description' => 'shanyvin limited Jiinue loan',
                'interest' => 5,
                'created_at' => Carbon::now()
            ],

            [
                'code' => time() + rand(1, 100000000),
                'name' => 'Inuka loan',
                'description' => 'shanyvin limited Inuka loan',
                'interest' => 16,
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
