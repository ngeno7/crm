<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'first_name' => 'admin',
            'last_name' => 'admin',
            'id_number' => '13233434',
            'email' => 'admin@sifafx.com',
            'password' => Hash::make('shanyvin123')
        ]);
    }
}
