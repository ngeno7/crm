<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
use Repo\Helpers\EnumConsts;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('loan_type_id')->unsigned();
            $table->string('code', 50);
            $table->string('loan_interest', 5)->default('0.00');
            $table->string('amount_disbursed', 20)->default('0.00');
            $table->dateTime('date_of_disbursement')->nullable();
            $table->string('loan_period', 15);
            $table->integer('period_type');
            $table->string('installment_amount', 20);
            $table->integer('installment_type');
            $table->string('remaining_payment', 20)->default('0.00');
            $table->string('loan_level', 15);
            $table->integer('status')->default(EnumConsts::STATUS_pending);
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();

            $table->foreign('client_id')
                  ->references('id')
                  ->on('clients');
            $table->foreign('loan_type_id')
                  ->references('id')
                  ->on('loan_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
