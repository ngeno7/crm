<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Repo\Helpers\EnumConsts;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 32);
            $table->string('first_name', 30);
            $table->string('last_name', 30);
            $table->string('email_address', 30);
            $table->string('id_number', 15);
            $table->string('phone_number', 15)->nullable();
            $table->string('address', 50)->nullable();
            $table->string('nok_name', 80)->nullable();
            $table->string('nok_email_address', 50)->nullable();
            $table->string('nok_phone_number', 30)->nullable();
            $table->integer('status')->default(EnumConsts::STATUS_pending);
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
