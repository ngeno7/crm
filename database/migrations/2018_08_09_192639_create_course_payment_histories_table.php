<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursePaymentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *'student_course_id', 'amount', 'transaction_code',
      *   'comment', 'status', 'payment_mode', 'deleted_at'
     * @return void
     */
    public function up()
    {
        Schema::create('course_payment_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 25);
            $table->integer('student_course_id')->unsigned();
            $table->string('amount', 15);
            $table->string('transaction_code', 30);
            $table->string('payment_mode', 15);
            $table->string('comment', 50)->nullable();
            $table->integer('status')->default(\Repo\Helpers\EnumConsts::STATUS_active);
            $table->timestamps();

            $table->foreign('student_course_id')
                    ->references('id')
                    ->on('student_courses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_payment_histories');
    }
}
