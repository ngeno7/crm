<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Repo\Helpers\EnumConsts;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 20)->unique();
            $table->string('first_name', 30);
            $table->string('last_name', 30);
            $table->string('middle_name', 30)->nullable();
            $table->string('membership_number', 15);
            $table->string('id_number', 15)->unique();
            $table->string('pin_number', 15)->unique();
            $table->string('email_address', 30);
            $table->string('phone_number', 12);
            $table->string('address', 50)->nullable();
            $table->string('company', 12)->nullable();
            $table->string('guarantor_name', 50)->nullable();
            $table->string('guarantor_id_number', 15)->nullable();
            $table->string('guarantor_phone_number', 15)->nullable();
            $table->string('guarantor_email_address', 30)->nullable();
            $table->string('guarantor_address', 50)->nullable();
            $table->integer('status')->default(EnumConsts::STATUS_pending);
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
