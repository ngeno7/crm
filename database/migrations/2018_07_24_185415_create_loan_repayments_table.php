<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Repo\Helpers\EnumConsts;
use Carbon\Carbon;

class CreateLoanRepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_repayments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50);
            $table->integer('loan_id')->unsigned();
            $table->string('payment_mode', 20);
            $table->string('amount', 20);
            $table->string('comment', 100)->nullable();
            $table->string('transaction_code', 30)->nullable();
            $table->string('proof_of_transaction')->nullable();
            $table->dateTime('payment_date')->default(Carbon::now());
            $table->integer('status')->default(EnumConsts::STATUS_pending);
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();

            $table->foreign('loan_id')
                  ->references('id')
                  ->on('loans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_repayments');
    }
}
