<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'first_name' => 'required|max:30',
            'last_name' => 'required|max:30',
            'id_number' => 'required|:clients|max:15',
            'pin_number' => 'required|unique:clients|max:15',
            'email_address' => 'required|max:30',
            'phone_number' => 'required|max:12'
        ];
    }
}
