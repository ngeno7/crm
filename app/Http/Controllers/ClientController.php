<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('layouts.pages.clients.index',
                 [ 'activeClients' => Client::latest()->active()->get(),
                    'pendingClients' => Client::latest()->pending()->get(),
                    'rejectedClients' => Client::latest()->rejected()->get()
                 ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('layouts.pages.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        //
        
        $data = $request->all();
        $data['code'] = uniqid();
        $data['membership_number'] = time() + rand(100, 10000);
        Client::create($data);
        session()->flash('flash_message', 'client added successfully');

        //redirect to the home page
        return redirect()->route('clients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit($code)
    {
        //
        return view('layouts.pages.clients.edit', ['client' => Client::where('code', $code)->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code)
    {
        //
        Client::where('code', $code)->firstOrFail()->update($request->all());
        session()->flash('flash_message', 'client updated successfully');

        //redirect to the home page
        return redirect()->route('clients.index');
    }
    public function updateStatus(Request $request, $code) 
    {
        // dd($request->all(),Client::where('code', $code)->firstOrFail());
        
        Client::where('code', $code)->first()->update(['status' => $request->status]);
        session()->flash('flash_message', 'Status updated successfully');

        return redirect()->route('clients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }
}
