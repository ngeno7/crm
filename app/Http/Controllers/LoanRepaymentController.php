<?php

namespace App\Http\Controllers;

use App\Models\LoanRepayment;
use Illuminate\Http\Request;

class LoanRepaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('layouts.pages.loan-repayments.index', 
        [
            'pendingRepayments' => LoanRepayment::with(['loan.client'])->pending()->latest()->get(),
            'activeRepayments' => LoanRepayment::with(['loan.client'])->active()->latest()->get(), 
            'rejectedRepayments' => LoanRepayment::with(['loan.client'])->rejected()->latest()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['code'] = uniqid();
        if($request->get('payment_date')) {
            $data['payment_date'] = Carbon::parse($request->get('payment_date'));
        }

        LoanRepayment::create($data);
        session()->flash('flash_message', 'repayment has been added pending approval.');

        return redirect()->back();
    }
    public function paymentReceipt($code)
    {
        $loan = LoanRepayment::with(['loan.loanType', 'loan.client'])->where('code', $code)->firstOrFail();

        $pdf = \PDF::loadView('layouts.docs.loans.receipt-loan', ['loan' => $loan ]);

        return $pdf->download(time().'_loan_receipt.pdf');
    }
}
