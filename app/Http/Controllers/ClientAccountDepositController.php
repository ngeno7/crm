<?php

namespace App\Http\Controllers;

use App\ClientAccountDeposit;
use Illuminate\Http\Request;

class ClientAccountDepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClientAccountDeposit  $clientAccountDeposit
     * @return \Illuminate\Http\Response
     */
    public function show(ClientAccountDeposit $clientAccountDeposit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClientAccountDeposit  $clientAccountDeposit
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientAccountDeposit $clientAccountDeposit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClientAccountDeposit  $clientAccountDeposit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientAccountDeposit $clientAccountDeposit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClientAccountDeposit  $clientAccountDeposit
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientAccountDeposit $clientAccountDeposit)
    {
        //
    }
}
