<?php

namespace App\Http\Controllers;

use App\Models\StudentCourse;
use App\Models\CoursePaymentHistory;
use Illuminate\Http\Request;

class StudentCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('layouts.pages.academy.student-course.index');
    }
    public function coursePayments()
    {
        return view('layouts.pages.academy.student-course.course-payments', ['payments' => CoursePaymentHistory::latest()->get()]);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['code'] = uniqid();
        \DB::transaction(function() use($data) {

            $studentCourse = StudentCourse::create($data);
            $data['code'] = time() + rand(1, 10000);
            $data['student_course_id'] = $studentCourse->id;
            CoursePaymentHistory::create($data);
        });
        
        session()->flash('flash_message', 'course registered successfully');

        return redirect()->back();
    }
    public function partialPayment(Request $request)
    {
        $data = $request->all();
        $data['code'] = time()+rand(1, 10000);
        CoursePaymentHistory::create($data);
        session()->flash('flash_message', 'course registered successfully');

        return redirect()->back();
        
    }
    public function paymentReceipt($code)
    {
        $coursePayment = CoursePaymentHistory::with([ 'studentCourse.course', 'studentCourse.student' ])->where('code', $code)->firstOrFail();
        $pdf = \PDF::loadView('layouts.docs.academy.receipt', ['coursePayment' => $coursePayment ]);

        return $pdf->download(time().'_course_payment.pdf');
    }
}
