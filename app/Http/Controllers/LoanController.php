<?php

namespace App\Http\Controllers;

use App\Models\LoanType;
use App\Models\Loan;
use App\Models\Client;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Repo\Helpers\EnumConsts;
use PDF;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('layouts.pages.loans.index', 
            [
                'activeLoans' => Loan::with(['loanType', 'client'])->active()->latest()->get(),
                'pendingLoans' => Loan::with(['loanType', 'client'])->pending()->get(),
                'rejectedLoans' => Loan::with(['loanType', 'client'])->rejected()->latest()->get()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $loanTypes = LoanType::latest()->get();
        if(request()->ajax()) {
            
            // return response()->json($loanTypes);
        }

        return view('layouts.pages.loans.create', [ 'loanTypes' => $loanTypes, 'clients' => Client::latest()->active()->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $data['code'] = uniqid();
        $data['date_of_disbursement'] = Carbon::parse($request->get('date_of_disbursement'));
        $data['end_of_loan_term'] = Carbon::parse($request->get('end_of_loan_term'));
        Loan::create($data);
        session()->flash('flash_message', 'Loan disbursed successfully.');

        return redirect()->route('loans.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        //
        return view('layouts.pages.loans.view', ['loan' => Loan::with(['client', 'loanType', 'repayments'])->where('code', $code)->first()]);
    }
    /**
     * loan update status
     */
    public function updateStatus(Request $request, $code)
    {
        $loan = Loan::where('code', $code)->firstOrFail();
        /**
         * @todo generate loan statement
         * generate loan statement
         */
        if($request->get('status') == EnumConsts::STATUS_active) {

        }
        /**
         * @todo mail notification/SMS
         */
        $loan->update($request->all());
        session()->flash('flash_message', 'Loan updated successfully');

        return redirect()->route('loans.index');
    }
    public function genLoanStatement($code)
    {
        
        $loan = Loan::with(['client', 'loanType'])->where('code', $code)->firstOrFail();

        $pdf = PDF::loadView('layouts.docs.loans.loan-statement', ['loan' => $loan ]);

        return $pdf->download(time().'_loan_statement.pdf');
    }
}
