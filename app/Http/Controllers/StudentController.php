<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Course;
use App\Models\CoursePaymentHistory;

use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('layouts.pages.academy.students.index', 
                [ 
                    'pendingStudents' => Student::pending()->latest()->get(),
                    'activeStudents' => Student::active()->latest()->get(),
                    'rejectedStudents' => Student::rejected()->latest()->get()
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('layouts.pages.academy.students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $data['code'] = time() + rand(15,9446564);

        Student::create($data);
        session()->flash('flash_message', 'student added successfully');

        return redirect()->route('students.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        //
        $student = Student::with(['courses'])->where('code', $code)->firstOrFail();
        $courses = [];
        if($student->courses) {
            $courses = array_map(function($st) {

                return $st['id'];
        }, $student->courses->toArray());
        }
        
        return view('layouts.pages.academy.students.view', 
                    [
                        'profile' => $student,
                        'courses' => Course::latest()->get(),
                        'coursePaymentHistory' => CoursePaymentHistory::with('studentCourse.course')->whereIn('student_course_id', $courses)->latest()->get()
                    ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }
}
