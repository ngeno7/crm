<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Repo\Helpers\EnumConsts;

class LoanRepayment extends Model
{
    //
    protected $fillable = [
        'loan_id', 'code', 'payment_mode', 
         'amount', 'comment','transaction_code',
          'proof_of_transaction','status',
          'payment_date', 'deleted_at'
        ];

    public function scopePending($query)
    {
        return $query->where('status', EnumConsts::STATUS_pending);
    }
    public function scopeActive($query)
    {
        return $query->where('status', EnumConsts::STATUS_active);
    }
    public function scopeRejected($query)
    {
        return $query->where('status', EnumConsts::STATUS_rejected);
    }

    public function loan() 
    {
        return $this->belongsTo(Loan::class);
    }
}
