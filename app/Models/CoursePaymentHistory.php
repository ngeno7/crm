<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoursePaymentHistory extends Model
{
    //
    protected $fillable = [
        'student_course_id', 'code', 'amount', 'transaction_code',
         'comment', 'status', 'payment_mode', 'deleted_at'
    ];

    public function studentCourse()
    {
        return $this->belongsTo(StudentCourse::class);
    }
}
