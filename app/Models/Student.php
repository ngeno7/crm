<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Repo\Helpers\EnumConsts;

class Student extends Model
{
    //
    protected $fillable = [
        'first_name', 'code', 'last_name', 'email_address',
         'id_number', 'phone_number', 'address', 'nok_name',
         'nok_email_address', 'nok_phone_number', 'status', 'deleted_at'
        ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function scopePending($query)
    {
        return $query->where('status', EnumConsts::STATUS_pending);
    }
    public function scopeActive($query)
    {
        return $query->where('status', EnumConsts::STATUS_active);
    }
    public function scopeRejected($query)
    {
        return $query->where('status', EnumConsts::STATUS_rejected);
    }

    public function courses()
    {
        return $this->hasMany(StudentCourse::class, 'student_id');
    }
}
