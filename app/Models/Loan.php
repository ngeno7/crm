<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Repo\Helpers\EnumConsts;

class Loan extends Model
{
    //
    protected $fillable = [
        'client_id', 'loan_type_id', 'code', 'loan_interest', 
        'amount_disbursed', 'date_of_disbursement',
         'loan_period', 'period_type', 'loan_level',
         'installment_amount', 'installment_type', 'remaining_payment',
         'status', 'deleted_at'
        ];
    
    protected $dates = ['created_at', 'date_of_disbursement', 'end_of_loan_term', 'deleted_at', 'updated_at'];
/**
 * client relationship
 */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    /**
     * loan types
     */
    public function loanType()
    {
        return $this->belongsTo(LoanType::class);
    }
    public function repayments()
    {
        return $this->hasMany(LoanRepayment::class, 'loan_id');
    }
    /**
     * query scopes going down
     */
    public function scopePending($query)
    {
        return $query->where('status', EnumConsts::STATUS_pending);
    }
    public function scopeActive($query)
    {
        return $query->where('status', EnumConsts::STATUS_active);
    }
    public function scopeRejected($query)
    {
        return $query->where('status', EnumConsts::STATUS_rejected);
    }
}
