<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanType extends Model
{
    //
    protected $fillable = ['name', 'code', 'interest', 'description', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at'];
}
