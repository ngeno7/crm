<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Repo\Helpers\EnumConsts;

class Client extends Model
{
    //
    protected $fillable = [
            'code', 'first_name', 'last_name', 'middle_number',
            'membership_number', 'id_number', 'pin_number',
            'email_address', 'phone_number', 'address', 
            'company', 'guarantor_name', 'guarantor_id_number',
            'guarantor_phone_number', 'guarantor_email_address',
            'guarantor_address',  'status', 'deleted_at'
        ];
    
    protected $date  = ['created_at', 'updated_at', 'deleted_at'];

    public function scopeActive($query) 
    {
        return $query->where('status', EnumConsts::STATUS_active);
    }
    public function scopePending($query) 
    {
        return $query->where('status', EnumConsts::STATUS_pending);
    }
    public function scopeRejected($query)
    {
        return $query->where('status', EnumConsts::STATUS_rejected);
    }
}
